const http = require('http')
const fs = require('fs')
const url = require('url')
const port = 8081

const requestHandler = (request, response) => {
  console.log(request.url)
  const resurl = url.pathToFileURL(request.url)
  var filepath = resurl.pathname
  filepath = filepath.split("%3F")[0]
  if(filepath == '/') filepath = "index.html"
  
  fs.readFile("./"+filepath,(err,data) => {
      if(err) {
          console.log(err)
          response.statusCode = 404
          response.end("Not Found")
      } else {
          response.end(data)
      }
  })
}

const server = http.createServer(requestHandler)

server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log(`server is listening on ${port}`)
})