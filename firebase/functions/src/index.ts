import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin'


// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

admin.initializeApp()


export const generateToken = functions.https.onCall((data,context) => {
    const uid = data.guid
    const claims = {
        guid: uid,
        pfm: true
    }
    return admin.auth().createCustomToken(uid,claims)
    .then((token) => {return token})
    .catch((error) => {throw new functions.https.HttpsError("internal",error)})
})



export const gettoken = functions.https.onRequest((req,res)=>{
    const uid = req.query.uid
    return admin.auth().createCustomToken(uid)
    .then((token) => res.send(token))
    .catch((error) => {throw new functions.https.HttpsError("internal",error)})   
})

export const storeSpending = functions.https.onCall((data,context) => {
    if(!data.aguid && !context.auth) throw new functions.https.HttpsError('failed-precondition','Authentication required')
    const uid = data.hasOwnProperty('aguid') ? data.aguid : context.auth.uid
    return admin.firestore().collection(`/users/${uid}/spendings`).doc(data.spid).set(data,{merge:true})
    .then((res) => {return true})
    .catch((error) => {throw new functions.https.HttpsError("internal",error)})
} )