package com.kovidinvo.trackspendings.fragments


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI

import com.kovidinvo.trackspendings.R
import kotlinx.android.synthetic.main.fragment_autorun_info.*
import android.content.ComponentName
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.kovidinvo.trackspendings.MainActivity


/**
 * A simple [Fragment] subclass.
 *
 */
class AutorunInfoFragment : Fragment() {
    private var firstTimeShown = true


    private val AUTO_START_INTENTS = arrayOf(
            Intent().setComponent(ComponentName("com.samsung.android.lool", "com.samsung.android.sm.ui.battery.BatteryActivity")),
            Intent("miui.intent.action.OP_AUTO_START").addCategory(Intent.CATEGORY_DEFAULT),
            Intent().setComponent(ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity")),
            Intent().setComponent(ComponentName("com.letv.android.letvsafe", "com.letv.android.letvsafe.AutobootManageActivity")),
            Intent().setComponent(ComponentName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity")),
            Intent().setComponent(ComponentName("com.coloros.safecenter", "com.coloros.safecenter.permission.startup.StartupAppListActivity")),
            Intent().setComponent(ComponentName("com.coloros.safecenter", "com.coloros.safecenter.startupapp.StartupAppListActivity")),
            Intent().setComponent(ComponentName("com.oppo.safe", "com.oppo.safe.permission.startup.StartupAppListActivity")),
            Intent().setComponent(ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.AddWhiteListActivity")),
            Intent().setComponent(ComponentName("com.iqoo.secure", "com.iqoo.secure.ui.phoneoptimize.BgStartUpManager")),
            Intent().setComponent(ComponentName("com.vivo.permissionmanager", "com.vivo.permissionmanager.activity.BgStartUpManagerActivity")),
            Intent().setComponent(ComponentName("com.asus.mobilemanager", "com.asus.mobilemanager.entry.FunctionActivity")).setData(Uri.parse("mobilemanager://function/entry/AutoStart"))
    )

    private var settingsIntent : Intent? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_autorun_info, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        btnOK.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_autorunInfoFragment_to_mapsFragment))

        val prefs = activity?.getPreferences(Context.MODE_PRIVATE)
        firstTimeShown = prefs?.getBoolean("autostartInfoFirstTime",true) ?: true
        prefs?.edit()?.putBoolean("autostartInfoFirstTime",false)?.apply()

        val strId = if(firstTimeShown) R.string.autostartFirstTime
            else R.string.autostartNextTime

        settingsIntent = AUTO_START_INTENTS.filter {activity?.packageManager?.resolveActivity(it,PackageManager.MATCH_DEFAULT_ONLY)!=null}.firstOrNull()
        if(settingsIntent==null) btnAutostartOpenSettings.visibility=View.GONE

        val vendorId = if(settingsIntent!=null) R.string.autostartKnownVendor else R.string.autostartUnknownVendor
        txtAutostartInfo.text = String.format("%s%s",getString(strId),getString(vendorId))
        btnAutostartOpenSettings.setOnClickListener { if(settingsIntent!=null) startActivity(settingsIntent) }

        phImportControls.visibility = if(firstTimeShown) View.GONE else View.VISIBLE

        btnImportSMS.setOnClickListener {
            val periodId = when(radioImportPeriod.checkedRadioButtonId) {
                R.id.periodDay -> R.string.periodDay1
                R.id.periodWeek -> R.string.periodWeek1
                R.id.periodMonth -> R.string.periodMonth
                else -> -1
            }
            if(periodId<0) return@setOnClickListener
            LocalBroadcastManager.getInstance(context!!)
                    .sendBroadcast(MainActivity.createImportSMSMessage(periodId))
            Navigation.findNavController(it).navigate(R.id.action_autorunInfoFragment_to_mapsFragment)
        }

        super.onActivityCreated(savedInstanceState)
    }



}
