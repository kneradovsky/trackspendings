package com.kovidinvo.trackspendings.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer

import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.viewmodels.TotalViewModel
import kotlinx.android.synthetic.main.fragment_total.*

class TotalFragment : Fragment() {

    companion object {
        fun newInstance() = TotalFragment()
    }


    var totalValue : Double =0.0
        set(val1) {
        field=val1
        total_balance.text=String.format(getString(R.string.amount_number_format),val1)
    }


    private lateinit var viewModel: TotalViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_total, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(parentFragment!!).get(TotalViewModel::class.java)
        viewModel.total.observe(this,Observer {total -> totalValue=total})
    }



}
