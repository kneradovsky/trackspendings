package com.kovidinvo.trackspendings.fragments

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager

import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.SpendNotification
import com.kovidinvo.trackspendings.adapters.SpendTypesHistogram
import com.kovidinvo.trackspendings.viewmodels.SpendTypeViewModel
import kotlinx.android.synthetic.main.fragment_spend_type.*

class SpendTypeFragment : Fragment() {

    companion object {
        fun newInstance() = SpendTypeFragment()
    }

    private val adapter = SpendTypesHistogram(emptyList())

    private val HistObserver = Observer<List<SpendNotification>> { data->
        if(data.size>0) typesNoDataBanner?.visibility=View.GONE
        adapter.replaceData(data)
        spendingTypesChart?.drawChart(data)
    }

    private lateinit var viewModel: SpendTypeViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_spend_type, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SpendTypeViewModel::class.java)
        viewModel.SpendHistogram.observe(activity!!,HistObserver)
        typesList.adapter=adapter
        typesList.layoutManager=LinearLayoutManager(context)
    }



}
