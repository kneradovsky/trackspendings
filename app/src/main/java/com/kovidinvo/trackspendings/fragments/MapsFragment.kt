package com.kovidinvo.trackspendings.fragments


import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.kovidinvo.trackspendings.*
import com.kovidinvo.trackspendings.adapters.AllSpendingTypeListAdapter
import com.kovidinvo.trackspendings.adapters.SpendingsAdapter
import com.kovidinvo.trackspendings.data.*
import com.kovidinvo.trackspendings.viewmodels.PeriodFilterViewModel
import com.kovidinvo.trackspendings.viewmodels.SpeedTypesViewModel
import com.kovidinvo.trackspendings.viewmodels.TotalViewModel
import com.kovidinvo.trackspendings.viewmodels.onDividerClickedListener
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.*
import com.yandex.mapkit.map.Map
import com.yandex.runtime.image.ImageProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.dialog_alltypes.view.*
import kotlinx.android.synthetic.main.fragment_maps.*
import kotlinx.android.synthetic.main.fragment_placemark_clicked.view.*
import org.joda.time.DateTime
import java.util.*


class MapsFragment : Fragment(), SpendingsListPresenter,CameraListener {
    val BAD_LOCATION_THRESHOLD = 100000.0
    private val BAD_LOCATION_VALUE = BAD_LOCATION_THRESHOLD+10
    private val TAG = "MapsFragment"
    private lateinit var vmPeriodFilter : PeriodFilterViewModel
    private lateinit var vmTotal : TotalViewModel
    private lateinit var vmSpeedTypes : SpeedTypesViewModel
    private var speedTypesSet : Set<String> = emptySet()

    private lateinit var locationClient : FusedLocationProviderClient
    lateinit var app : App

    private var mapObjects : MapObjectCollection? = null
    private var listenerObjects : MutableList<MapObjectTapListener> = mutableListOf()
    private var selectLastItem = true

    private lateinit var viewPlaceMarkClicked : View
    private var lastPlaceMarkClicked : PlacemarkMapObject? = null

    private var showMapOnly = false
    private var autostartInfoShown = 0L
    private var lastSMSReceivedAt = 0L

    private val locationShifter = LocationShifter.instance

    val defaultMarkImageProvider : ImageProvider by lazy {
        ImageProvider.fromResource(context!!,R.drawable.abc_ic_star_black_16dp)
    }


    //Spendings list presenter
    override val isTypeImageClickSupported = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG,"onCreate")
        MapKitFactory.setApiKey("d69c7274-8223-4b69-81c1-5449f5819aaa")
        MapKitFactory.initialize(context!!)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.d(TAG,"onActivityCreated");
        app = (activity as MainActivity).app
        vmPeriodFilter =    ViewModelProviders.of(this).get(PeriodFilterViewModel::class.java)
        vmTotal = ViewModelProviders.of(this).get(TotalViewModel::class.java)
        vmPeriodFilter.period.observe(this,Observer {pair -> readSMS(pair.first?.time?:0,pair.second?.time?:Date().time)})
        vmPeriodFilter.dividerClickListener = object : onDividerClickedListener {
            override fun onDividerClick() {
                if(showMapOnly) {
                    horsplitter.setGuidelinePercent(0.5f)
                    showMapOnly=false
                } else {
                    horsplitter.setGuidelinePercent(0.96f)
                    showMapOnly=true
                }
            }
        }

        vmSpeedTypes = ViewModelProviders.of(activity!!).get(SpeedTypesViewModel::class.java)
        vmSpeedTypes.speedTypes.observe(this, Observer { v -> speedTypesSet = v })

        locationClient = LocationServices.getFusedLocationProviderClient(context!!)

        //store new messages to the database
        app.subjSMS.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe { notif ->
                    if((notif.longitude ?: BAD_LOCATION_VALUE)>BAD_LOCATION_THRESHOLD) return@subscribe //nothing to do if longitude is null or >100000
                    if(notif.mark==null) {
                        vmTotal.total.postValue((vmTotal.total.value ?: 0.0) + notif.amount)
                        addLocationMark(notif, false)
                    } else {
                        notif.mark?.setIcon(app.imageProviders[notif.type]?: defaultMarkImageProvider) //update mark to type
                    }
                } != null




    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d(TAG,"onCreateView");
        viewPlaceMarkClicked = inflater.inflate(R.layout.fragment_placemark_clicked,container,false)
        return inflater.inflate(R.layout.fragment_maps,container,false)
    }



    override fun onResume() {
        super.onResume()
        Log.d(TAG,"onResume");

    }

    override fun onStop() {
        super.onStop()
        yandexMap.map.removeCameraListener(this)
        yandexMap.onStop();
        MapKitFactory.getInstance().onStop()

    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG,"onPause");

    }


    override fun onStart() {
        super.onStart()
        Log.d(TAG,"onStart");

        MapKitFactory.getInstance().onStart()
        yandexMap.onStart()
        //initMarksCollection(reinit=true)
        yandexMap.map.addCameraListener(this)

    }

    fun readSMS(start:Long,end:Long) {
        Log.d(TAG,"readSMS")
        app.spendDb.spendingsDao().getSpendingsByDates(start,end)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map {list -> list.toMutableList()}
                .subscribe { smslist ->
                    var total = 0.0
                    val adapter = SpendingsAdapter(smslist, app.subjSMS, this)
                    listSpendings.adapter = adapter
                    listSpendings.layoutManager = LinearLayoutManager(activity)
                    initMarksCollection(true)

                    smslist.forEachIndexed {
                        i,notif ->
                        if((notif.lattitude ?: BAD_LOCATION_VALUE)<BAD_LOCATION_THRESHOLD) { //don't add placemark if coordinates is out of range or null
                            locationShifter?.shiftLocation(notif) //shift location in shifter is not null. Useful for promo video recordings
                            addLocationMark(notif,false)
                        }
                        total+=notif.amount
                    }

                    vmTotal.total.postValue(total)
                    if(smslist.size > 0) {
                        onListItemClick(smslist[0])
                        lastSMSReceivedAt = smslist[0].date.time
                    }
                    selectLastItem=false
                    showAutostartInfoFragment()
                } != null
    }

    fun initMarksCollection(reinit:Boolean =false) {
        Log.i(TAG,"initMarks Called")
        if(yandexMap==null) return
        if(mapObjects!=null) mapObjects!!.clear()
        if(mapObjects==null || reinit) mapObjects = yandexMap.map.mapObjects.addCollection()
        listenerObjects.clear()
    }

    fun getTapListener(sp:SpendNotification) : MapObjectTapListener {
        val listener =  MapObjectTapListener { mapObject:MapObject, point:Point ->
            val m = mapObject as PlacemarkMapObject
            if(lastPlaceMarkClicked!=null) clickOnPlaceMark(lastPlaceMarkClicked!!)

            if(lastPlaceMarkClicked!=m) {
                clickOnPlaceMark(m)
                lastPlaceMarkClicked = m
            } else lastPlaceMarkClicked=null

            true
        }
        listenerObjects.add(listener)
        return listener
    }

    private fun clickOnPlaceMark(pm: PlacemarkMapObject) {
        val pmdata = pm.userData as PlacemarkData
        if(pmdata.clicked) {
            pm.setIcon(app.imageProviders[pmdata.sp.type]?: defaultMarkImageProvider)
            pmdata.clicked=false
        } else {
            fillPlacemarkView(pmdata.sp)
            pm.setIcon(ImageProvider.fromBitmap(ViewImageProvider.getBitmapFromView(viewPlaceMarkClicked),false,pmdata.sp.spid))
            pmdata.clicked = true
        }
    }

    fun addLocationMark(sp: SpendNotification,moveCamera:Boolean) {
        //shift marks for video recording
        val p = Point(sp.lattitude!!,sp.longitude!!)
        if(yandexMap==null) {
            Log.d(TAG,"yandex maps is null")
            return
        }
        val mark = mapObjects!!.addPlacemark(p)
        mark.opacity = 1.0f
        mark.isDraggable = false
        mark.addTapListener(getTapListener(sp))
        mark.userData=PlacemarkData(false,sp)
        mark.setIcon(app.imageProviders[sp.type]?: defaultMarkImageProvider)
        sp.mark=mark
        val zoom = if((yandexMap.map?.cameraPosition?.zoom ?: 0.0f) >10.0f) yandexMap.map.cameraPosition.zoom else 11.0f
        if(moveCamera) yandexMap.map.move(CameraPosition(p,zoom, 0.0f, 0.0f))
    }


    override fun scrollToPosition(pos: Int) {
        listSpendings?.scrollToPosition(pos)
    }

    override fun onListItemClick(sp: SpendNotification) {
        if((sp.lattitude ?: BAD_LOCATION_VALUE)>BAD_LOCATION_THRESHOLD) return
        val zoom = if((yandexMap.map?.cameraPosition?.zoom ?: 0.0f) >10.0f) yandexMap.map.cameraPosition.zoom else 11.0f
        val cp = CameraPosition(Point(sp.lattitude!!,sp.longitude!!),zoom,0.0f,0.0f)
        yandexMap.map.move(cp)
    }

    override fun onTypeImageClick(sp: SpendNotification) {
        //build dialog
        val builder = AlertDialog.Builder(activity,R.style.AlertDialogTheme)
        builder.setPositiveButton(R.string.btnYes) {dialog,id -> updatePOSType(sp)}
        builder.setNegativeButton(R.string.btnNo) {dialog,id ->
            AsyncTask.execute {
                val prevType = app.spendDb.spendingsDao().getById(sp.spid)?.type ?: ""
                app.updateBudgets(sp,prevType)
                app.subjSMS.onNext(sp)
            }
            app.updateSpendingFirebase(sp)
            sp.mark?.setIcon(app.imageProviders[sp.type]?:defaultMarkImageProvider)
        }
        val message = String.format(getString(R.string.prompt_change_type_all_spendings,
                getString(SpendTypeData.resStrIds[sp.type]!!),sp.pos))
        builder.setMessage(message)
        builder.create().show()
    }

    @SuppressLint("CheckResult")
    private fun updatePOSType(sp:SpendNotification) {
        val pos = POS(name=sp.pos,type=sp.type,lattitude = sp.lattitude, longitude =  sp.longitude)
        app.spendDb.queryExecutor.execute {
            app.spendDb.posDao().insertPos(pos)
            val updated = app.spendDb.spendingsDao().updateAllSpendingTypeInPos(sp.pos,sp.type)
            if(updated==0) return@execute
            val period = vmPeriodFilter.period.value
            app.spendDb.updateAllActiveBudgets()
            readSMS(period?.first?.time ?:0,period?.second?.time ?: Date().time) //reread sms
        }
    }
    override fun onCameraPositionChanged(m: Map, cp: CameraPosition, cs: CameraUpdateSource, finished: Boolean) {
        if(!finished) return
        val region = m.visibleRegion(cp)
        fun inRegion(p:Point) : Boolean {
            if(p.latitude>region.topLeft.latitude || p.latitude<region.bottomRight.latitude) return false
            if(p.longitude<region.topLeft.longitude || p.longitude>region.bottomRight.longitude) return false
            return true
        }
        val total = (listSpendings.adapter as SpendingsAdapter).data
                .filter { sp -> sp.longitude!=null && sp.lattitude!=null}
                .filter { sp -> inRegion(Point(sp.lattitude!!,sp.longitude!!))}
                .sumByDouble { sp -> sp.amount }
        mapAmountTotal.text = getString(R.string.amount_on_map_prologue)+String.format(getString(R.string.amount_number_format),total)
    }

    fun fillPlacemarkView(sp:SpendNotification) {

        viewPlaceMarkClicked.layoutMarkLarge.background = if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP) {
            context!!.getDrawable(SpendTypeData.bgLargeIds[sp.type]?:R.drawable.background_unknown_large)
        } else {context!!.resources.getDrawable(SpendTypeData.bgLargeIds[sp.type]?:R.drawable.background_unknown_large)}
        viewPlaceMarkClicked.imgBankLogo.setImageResource(BankData.logosList[sp.bank]?:R.drawable.ic_banklogo_unknown)
        viewPlaceMarkClicked.txtCardNum.text = FormatUtils.formatCardNum(sp.card)
        viewPlaceMarkClicked.txtAmount.text = FormatUtils.formatAmount(sp.amount)
        viewPlaceMarkClicked.txtPOS.text = sp.pos
        val operDate = DateTime(sp.date)
        if(operDate.withTimeAtStartOfDay()== DateTime.now().withTimeAtStartOfDay()) {
            //today
            viewPlaceMarkClicked.txtDate.text=""
        } else {
            viewPlaceMarkClicked.txtDate.text = operDate.toString("dd.MM.yyyy")
            //other day
        }
        viewPlaceMarkClicked.txtTime.text = operDate.toString("HH:mm")
    }

    private fun showAutostartInfoFragment() {
        val showThreshold = 86400*1*1000
        val curTime = Date().time
        if(curTime - lastSMSReceivedAt<showThreshold) {
            return //nothing to show
        }
        val prefs = activity?.getPreferences(Context.MODE_PRIVATE)
        if(prefs==null) {
            Log.d(TAG,"preferences couldn't be loaded")
            return
        }
        //val donotShow = prefs.getBoolean("autostartInfoDontShow",false)
        //if(donotShow) return //don't show autoinfo checked
        autostartInfoShown = prefs.getLong("autostartInfoShown",0L)
        if (curTime-autostartInfoShown > showThreshold) {
            prefs.edit().putLong("autostartInfoShown",curTime).apply()
            NavHostFragment.findNavController(this).navigate(R.id.action_mapsFragment_to_autorunInfoFragment)
        }
    }

    override fun selectFromAllTypes(sp: SpendNotification) {
        val inflater = activity!!.layoutInflater
        val spendTypeData = SpendingType.values().map {v -> mutableMapOf("id" to v.n,"text" to  resources.getString(SpendTypeData.resStrIds[v.n]!!),"image" to SpendTypeData.resIds[v.n]!!)}
        val adapter = AllSpendingTypeListAdapter(spendTypeData)
        adapter.lastSelectedType=sp.type
        val view = inflater.inflate(R.layout.dialog_alltypes,null)

        val builder = AlertDialog.Builder(activity,R.style.AlertDialogTheme)
        builder.setTitle("Выберите тип")
        builder.setView(view)
                .setPositiveButton(R.string.btnYes) { dialog,id -> if(adapter.lastSelectedType!=sp.type && adapter.lastSelectedType!=null) {
                        sp.type=adapter.lastSelectedType!!
                        onTypeImageClick(sp)
                    }
                }
                .setNegativeButton(R.string.btnCancel) {dialog,id -> }

        val dialog = builder.create()
        val list = view.listSpendTypes
        list.layoutManager=LinearLayoutManager(context)
        list.adapter=adapter
        dialog.show()
    }

    override fun getPreferredTypes(): Set<String> = speedTypesSet
}

data class PlacemarkData(var clicked:Boolean,val sp:SpendNotification)

interface SpendingsListPresenter {
    fun scrollToPosition(pos:Int)
    fun onListItemClick(sp: SpendNotification)
    fun onTypeImageClick(sp: SpendNotification)
    fun getPreferredTypes() : Set<String> = emptySet()
    fun selectFromAllTypes(sp:SpendNotification) {}
    val isTypeImageClickSupported : Boolean
}




