package com.kovidinvo.trackspendings.fragments

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.viewmodels.PeriodFilterViewModel
import io.reactivex.functions.Function
import kotlinx.android.synthetic.main.fragment_period_filter.*
import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.text.SimpleDateFormat
import java.util.*


class PeriodFilterFragment : Fragment() {
    private val dateFormat = "%02d.%02d.%d"
    val dateSetListener = Function<Int,DatePickerDialog.OnDateSetListener> {id ->
        val datePicker = when(id) {
            R.id.dateStart -> dateStart
            R.id.dateEnd -> dateEnd
            else -> null
        }
        val ret = DatePickerDialog.OnDateSetListener { dp,year,month,day ->
            periodSelection.clearCheck()
            datePicker?.setText(String.format(dateFormat,day,month+1,year))
            updateModel()
        }
        return@Function ret
    }

    val dateOnClickListener = View.OnClickListener { view ->
        val dateStr = (view as TextView).text
        val dateRx = Regex("^(\\d{2})\\.(\\d{2})\\.(\\d{4})$")
        val match = dateRx.matchEntire(dateStr)
        val date = if(match!=null) {
            val d=match.groupValues.slice(1..3).map { d -> d.toInt() }.toMutableList()
            d[1]-=1
            d
        } else {
            val c = Calendar.getInstance()
            mutableListOf(c.get(Calendar.DAY_OF_MONTH),c.get(Calendar.MONTH),c.get(Calendar.YEAR))
        }
        val dp = DatePickerDialog(context,dateSetListener.apply(view.id),date[2],date[1],date[0])
        dp.show()
    }

    companion object {
        fun newInstance() = PeriodFilterFragment()
    }

    private lateinit var viewModel: PeriodFilterViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_period_filter, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(parentFragment!!).get(PeriodFilterViewModel::class.java)
        cntFilter.visibility=View.GONE
        //default period - 30 days
        setBackPeriod(30)
        periodSelection.check(R.id.rb_month)
        cntButtons.setOnClickListener { view -> showHideFilters() }
        periodSelection.setOnCheckedChangeListener { grp, i ->
            when(i) {
                R.id.rb_day -> setBackPeriod(1)
                R.id.rb_week -> setBackPeriod(7)
                R.id.rb_month -> setBackPeriod(30)
                else -> {}
            }
        }
        dateStart.setOnClickListener(dateOnClickListener)
        dateEnd.setOnClickListener(dateOnClickListener)
        imgLineDivider.setOnClickListener { view ->
            viewModel.dividerClickListener?.onDividerClick()
        }
        updateModel()
    }

    fun showHideFilters() {
        val isShown = cntFilter.isShown
        val animations = if(isShown) Pair(R.anim.slide_up,R.anim.rotate_180_0) else Pair(R.anim.slide_down,R.anim.rotate_0_180)
        val slide = AnimationUtils.loadAnimation(context,animations.first).also {
            anim ->
            anim.reset()
            cntFilter.startAnimation(anim)
        }
        val rotate = AnimationUtils.loadAnimation(context,animations.second).also {
            anim ->
            anim.reset()
            imgArrow.startAnimation(anim)
        }
        if(isShown) cntFilter.visibility=View.GONE
        else cntFilter.visibility=View.VISIBLE

    }

    fun updateModel() {
        viewModel.invalidate(this)
        lblPeriod.text=viewModel.caption
    }

    fun setBackPeriod(days:Int) {
        val df = SimpleDateFormat("dd.MM.yyyy")
        val c = Calendar.getInstance()
        dateEnd.setText(df.format(c.time))
        c.add(Calendar.DATE,-1*(days-1))
        dateStart.setText(df.format(c.time))
        updateModel()
    }
}
