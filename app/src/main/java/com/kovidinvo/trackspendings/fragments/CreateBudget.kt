package com.kovidinvo.trackspendings.fragments

import android.app.DatePickerDialog
import android.os.AsyncTask
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.SimpleAdapter
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.kovidinvo.trackspendings.App
import com.kovidinvo.trackspendings.Budget
import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.adapters.BudgetCategoriesListAdapter
import com.kovidinvo.trackspendings.data.SpendTypeData
import com.kovidinvo.trackspendings.data.SpendingType
import io.reactivex.functions.Function
import kotlinx.android.synthetic.main.fragment_create_budget.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class CreateBudget : Fragment() {


    lateinit var listitemsCategories : List<MutableMap<String,Any>>

    lateinit var catTypeAdaper : BudgetCategoriesListAdapter
    val dateFormat = SimpleDateFormat("dd.MM.yyyy")
    private val printDateFormat = "%02d.%02d.%d"


    companion object {
        fun newInstance() = CreateBudget()
    }

    val dateSetListener = Function<Int, DatePickerDialog.OnDateSetListener> { id ->
        val datePicker = when(id) {
            R.id.budgetStart -> budgetStart
            R.id.budgetEnd -> budgetEnd
            else -> null
        }
        val ret = DatePickerDialog.OnDateSetListener { dp, year, month, day ->
            datePicker?.setText(String.format(printDateFormat,day,month+1,year))
        }
        return@Function ret
    }

    val dateOnClickListener = View.OnTouchListener() { view,me ->
        if(me.action!=MotionEvent.ACTION_UP) return@OnTouchListener true
        val dateRx = Regex("^(\\d{2})\\.(\\d{2})\\.(\\d{4})$")
        val match = dateRx.matchEntire((view as TextView).text)
        val date = if(match!=null) {
            val d=match.groupValues.slice(1..3).map { d -> d.toInt() }.toMutableList()
            d[1]-=1
            d
        } else {
            val c = Calendar.getInstance()
            mutableListOf(c.get(Calendar.DAY_OF_MONTH),c.get(Calendar.MONTH),c.get(Calendar.YEAR))
        }
        val dp = DatePickerDialog(context,dateSetListener.apply(view.id),date[2],date[1],date[0])
        dp.show()
        true
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_create_budget, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        budgetStart.setOnTouchListener(dateOnClickListener)
        budgetEnd.setOnTouchListener(dateOnClickListener)
        btnCancel.setOnClickListener { onCancel() }
        btnCreateBudget.setOnClickListener { onCreate() }
        listitemsCategories = SpendingType.values()
                .map {v -> mutableMapOf("id" to v.n,"text" to  resources.getString(SpendTypeData.resStrIds[v.n]!!),"image" to SpendTypeData.resIds[v.n]!!,"checked" to false)}
        catTypeAdaper = BudgetCategoriesListAdapter(listitemsCategories)
        listCategories.adapter=catTypeAdaper
        listCategories.layoutManager=LinearLayoutManager(context)
    }


    private fun onCancel() {
        view?.findNavController()?.navigate(R.id.action_createBudget_to_budgetFragment)
    }

    private fun onCreate() {
        val app = activity!!.application as App
        val requiredFields = listOf(Pair(budgetName,layoutName),Pair(budgetStart,layoutStart),
                                    Pair(budgetEnd,layoutEnd),Pair(budgetAmount,layoutAmount))
        var hasErrors = requiredFields
                .filter { p -> p.first.text?.isEmpty() ?: true }
                .onEach { p -> p.second.error=getString(R.string.errorEmptyField);p.second.isErrorEnabled=true }.size
        var categories = listitemsCategories.filter { item -> item["checked"] as Boolean}.map { item -> item["id"].toString()}.toList()
        if(categories.size == 0) {
            hasErrors+=1
            Snackbar.make(view!!,R.string.errorBudgetNoCategories,Snackbar.LENGTH_SHORT)
                    .show()
        }
        var dateStart = Date()
        try {dateStart = dateFormat.parse(budgetStart.text.toString())}
        catch(e:ParseException) {hasErrors+=1;layoutStart.isErrorEnabled=true}
        if(dateStart.time>Date().time) {
            hasErrors+=1
            layoutStart.error=getString(R.string.errorStartDateInFuture)
            layoutStart.isErrorEnabled=true
        }
        var dateEnd = dateStart
        try {dateEnd = dateFormat.parse(budgetEnd.text.toString());dateEnd=Date(dateEnd.time+24*3600*1000)}
        catch(e:ParseException) {hasErrors+=1;layoutEnd.isErrorEnabled=true}
        if(hasErrors==0) {
            //create budget
            val budget = Budget(
                    name= budgetName.text.toString(),
                    start = dateStart,
                    end = dateEnd,
                    type = categories.toMutableList(),
                    planned = budgetAmount.text.toString().toDouble(),
                    actual = 0.0,
                    periodtype = "custom",
                    active = 1
            )
            if(countExistingSpendings.isChecked) {
                app.spendDb.queryExecutor.execute {
                    budget.actual = app.spendDb.getBudgetCurrentSpendings(budget)
                    app.spendDb.budgetDao().insert(budget)
                }
            } else app.spendDb.queryExecutor.execute {app.spendDb.budgetDao().insert(budget)}
            view?.findNavController()?.navigate(R.id.action_createBudget_to_budgetFragment)
        }

    }

}
