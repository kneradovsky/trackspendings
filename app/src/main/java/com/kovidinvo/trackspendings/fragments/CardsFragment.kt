package com.kovidinvo.trackspendings.fragments

import android.graphics.Color
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.DefaultValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import com.kovidinvo.trackspendings.PayCard
import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.adapters.CardListPresenter
import com.kovidinvo.trackspendings.adapters.CardsList
import com.kovidinvo.trackspendings.data.BankData
import com.kovidinvo.trackspendings.data.PeriodType
import com.kovidinvo.trackspendings.data.SpendTypeData
import com.kovidinvo.trackspendings.viewmodels.CardsViewModel
import com.kovidinvo.trackspendings.viewmodels.StatsSpendType
import kotlinx.android.synthetic.main.fragment_cards.*


class CardsFragment : Fragment(), CardListPresenter {


    private val TAG ="CardsFragment"
    private var prevPressed : ImageButton? = null

    val adapter = CardsList(emptyList(),this)

    val mediumTypeFace by lazy { ResourcesCompat.getFont(context!!,R.font.roboto_medium) }
    val regularTypeFace by lazy { ResourcesCompat.getFont(context!!,R.font.roboto_light) }

    companion object {
        fun newInstance() = CardsFragment()
    }

    private lateinit var viewModel: CardsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_cards, container, false)
    }

    private val cardsObserver = Observer<List<PayCard>> { newdata ->
        if(newdata.size>0) cardsNoDataBanner?.visibility=View.GONE
        adapter.replaceData(newdata)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(CardsViewModel::class.java)
        cardList.adapter = adapter
        cardList.layoutManager = LinearLayoutManager(activity)
        initBalanceChart()
        viewModel.cards.observe(this, cardsObserver)
        viewModel.typeHistogram.observe(this, Observer { data -> drawPiechart(data) })
        viewModel.updateStatData(emptyList())
//        btnChartDay.setOnClickListener { b -> pressPointButton(b as ImageButton) }
//        btnChartWeek.setOnClickListener { b -> pressPointButton(b as ImageButton) }
//        btnChartMonth.setOnClickListener { b -> pressPointButton(b as ImageButton) }
//        pressPointButton(btnChartDay)
    }

    private fun setPointValue(pt:PeriodType) {

    }


    private fun pressPointButton(btn: ImageButton) {
        prevPressed?.isSelected=false
        btn.isSelected=true
        prevPressed=btn
        val pt = when(btn.id) {
            R.id.btnChartDay -> PeriodType.DAY
            R.id.btnChartWeek -> PeriodType.WEEK
            R.id.btnChartMonth -> PeriodType.MONTH
            else -> PeriodType.DAY
        }
        setPointValue(pt)
    }

    private fun initBalanceChart() {
        with(balanceChart) {
            setUsePercentValues(false)
            description.isEnabled=false
            isDrawHoleEnabled=true
            setHoleColor(Color.WHITE)
            setDrawCenterText(true)
            rotationAngle=0.0f
            val l = legend
            l.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
            l.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT
            l.orientation = Legend.LegendOrientation.HORIZONTAL
            l.setDrawInside(false)
            l.isWordWrapEnabled=true
            l.xEntrySpace = 7f
            l.yEntrySpace = 5f
            l.yOffset = 0f
            // entry label styling
            setEntryLabelColor(ResourcesCompat.getColor(context!!.resources,R.color.coolGray,null))
            setEntryLabelTypeface(regularTypeFace)
            setEntryLabelTextSize(12f)
        }
        drawPiechart(mutableMapOf())
    }

    private fun drawPiechart(data:StatsSpendType) {
        var colors = mutableListOf<Int>()
        var totalAmount = 0.0
        val pdata = data.map { entry ->
            colors.add(SpendTypeData.typeColors[entry.key]!!)
            totalAmount += entry.value
            PieEntry(entry.value.toFloat(), getString(SpendTypeData.resStrIds[entry.key]
                    ?: R.string.nametype_unknown),
                    ResourcesCompat.getDrawable(context!!.resources, SpendTypeData.resIds[entry.key]
                            ?: R.drawable.ic_type_unknown, null)
            )
        }.toList()
        val dataset = PieDataSet(pdata, "")
        dataset.colors = colors
        dataset.iconsOffset = MPPointF(0f, 40f)
        dataset.selectionShift = 5f
        val piedata = PieData(dataset)
        piedata.setValueFormatter(DefaultValueFormatter(2))
        piedata.setValueTextSize(11f)
        piedata.setValueTextColor(Color.WHITE)
        piedata.setValueTypeface(regularTypeFace)
        balanceChart.data = piedata
        balanceChart.centerText=String.format(getString(R.string.formatShortTotalAmount),totalAmount)
        balanceChart.setCenterTextSize(15f)
        balanceChart.invalidate()
        balanceChart.animateY(1000, Easing.EaseInOutQuad)
    }

    override fun onCardStatChange(num:Int,state: Boolean) {
        viewModel.updateCardStat(num,!state)
    }
}
