package com.kovidinvo.trackspendings.fragments

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.kovidinvo.trackspendings.*
import com.kovidinvo.trackspendings.adapters.SpeedTypesListAdapter
import com.kovidinvo.trackspendings.data.SpendTypeData
import com.kovidinvo.trackspendings.data.SpendingType
import com.kovidinvo.trackspendings.databinding.FragmentSettingsBinding
import com.kovidinvo.trackspendings.viewmodels.SpeedTypesViewModel
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : Fragment() {

    private lateinit var app: App
    private val TAG = "SettingsActivity"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        app = (activity as MainActivity).app
        var binding: FragmentSettingsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false)
        val selfview = binding.root
        return selfview
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        versionName.text = BuildConfig.VERSION_NAME
        deviceInfo.text = Build.MANUFACTURER + "/" + Build.MODEL + "/" + Build.VERSION.RELEASE
        val speedtypesvm = ViewModelProviders.of(activity!!).get(SpeedTypesViewModel::class.java)

        val data = SpendingType.values()
                .map {v -> mutableMapOf(
                        "id" to v.n,
                        "text" to  resources.getString(SpendTypeData.resStrIds[v.n]!!),
                        "image" to SpendTypeData.resIds[v.n]!!,
                        "checked" to speedtypesvm.speedTypes.value!!.contains(v.n)
                )
                }

        listSpeedTypes.adapter = SpeedTypesListAdapter(data,speedtypesvm)
        listSpeedTypes.layoutManager = LinearLayoutManager(context)

        speedtypesvm.speedTypes.observe(this, Observer { v ->
            val types = v.map { getString(SpendTypeData.resStrIds[it] ?: R.string.nametype_unknown) }.joinToString(",")
            txtSpeedTypesSelected.text = String.format(getString(R.string.settings_selected_speedtypes),types)
        })

        btnShowOnboarding.setOnClickListener {
            val intent = Intent(context,OnBoarding::class.java).apply { putExtra("startedByUser",true) }
            startActivity(intent)
        }
    }


}
