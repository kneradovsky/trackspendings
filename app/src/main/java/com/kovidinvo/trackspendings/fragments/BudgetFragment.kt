package com.kovidinvo.trackspendings.fragments

import android.os.AsyncTask
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.kovidinvo.trackspendings.Budget
import com.kovidinvo.trackspendings.MainActivity

import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.SpendNotification
import com.kovidinvo.trackspendings.adapters.BudgetPresenter
import com.kovidinvo.trackspendings.adapters.BudgetsListAdapter
import com.kovidinvo.trackspendings.adapters.SpendingsAdapter
import com.kovidinvo.trackspendings.adapters.SwipeToDeleteHandler
import com.kovidinvo.trackspendings.viewmodels.BudgetViewModel
import com.kovidinvo.trackspendings.viewmodels.CreateBudgetViewModel
import com.kovidinvo.trackspendings.viewmodels.TotalViewModel
import kotlinx.android.synthetic.main.fragment_budget.*
import kotlinx.coroutines.coroutineScope
import java.text.SimpleDateFormat
import kotlin.coroutines.coroutineContext

class BudgetFragment : Fragment(), SpendingsListPresenter, BudgetPresenter {

    override val isTypeImageClickSupported = false
    private val dateFormat = SimpleDateFormat("dd.MM.yyyy")



    companion object {
        fun newInstance() = BudgetFragment()
    }

    private lateinit var viewModel: BudgetViewModel
    private lateinit var vmCreateBudget : CreateBudgetViewModel
    private lateinit var vmTotal : TotalViewModel

    private lateinit var budgetListAdapter : BudgetsListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_budget, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(BudgetViewModel::class.java)
        vmCreateBudget = ViewModelProviders.of(this).get(CreateBudgetViewModel::class.java)
        vmTotal = ViewModelProviders.of(this).get(TotalViewModel::class.java)

        fabAddBudget.setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_budgetFragment_to_createBudget)
        }
        budgetListAdapter = BudgetsListAdapter(viewModel.activeBudgets.value?: emptyList(),viewModel.pastBudgets.value ?: emptyList(),this)
        budgetsList.adapter = budgetListAdapter
        budgetsList.layoutManager = LinearLayoutManager(context)
        viewModel.activeBudgets.observe(this, Observer { list -> budgetListAdapter.dataActive=list })
        viewModel.pastBudgets.observe(this, Observer { list -> budgetListAdapter.dataPast = list })

        viewModel.budgetSpendings.observe(this, Observer { sps ->
            val adapter = SpendingsAdapter(sps.toMutableList(),null,this,R.layout.item_spendings_empty_budgets)
            spendingsList.adapter = adapter
            vmTotal.total.value = sps.map {it.amount}.sum()
        })
        //item touch helper
        val itemTouchHelper = ItemTouchHelper(SwipeToDeleteHandler(context!!) { viewHolder ->
            val itemPos = viewHolder.adapterPosition
            val itemType = viewHolder.itemViewType
            val budget = when(itemType) {
                3 -> if(itemPos - 1 < budgetListAdapter.dataActive.size)
                        budgetListAdapter.dataActive[itemPos-1]
                     else null
                4 -> {
                    val pos = itemPos - 2 - budgetListAdapter.dataActive.size
                    if(pos<budgetListAdapter.dataPast.size)
                        budgetListAdapter.dataPast[pos]
                    else null
                }
                else -> null
            }
            if(budget!=null) {
                viewModel.app.spendDb.queryExecutor.execute {
                    viewModel.app.spendDb.budgetDao().delete(budget)
                }
            } else budgetListAdapter.notifyItemChanged(itemPos)
        }.apply { pinnedTypes = listOf(1,2) } )
        itemTouchHelper.attachToRecyclerView(budgetsList)

        //spendings list
        spendingsList.adapter = SpendingsAdapter(mutableListOf(),null,this,R.layout.item_spendings_empty_budgets)
        spendingsList.layoutManager=LinearLayoutManager(context!!)

        //heading
        headingDateEnd.text=""
        headingDateStart.text=""

    }

    override fun onResume() {
        super.onResume()
        LocalBroadcastManager.getInstance(context!!)
                .sendBroadcast(MainActivity.createBottomBadgeMessage(R.id.budgetFragment))
    }

    override fun scrollToPosition(pos: Int) {
    }

    override fun onListItemClick(sp: SpendNotification) {
    }

    override fun onTypeImageClick(sp: SpendNotification) {
    }

    override fun onBudgetClick(b: Budget) {
        headingName.text = b.name
        headingDateStart.text = dateFormat.format(b.start)
        headingDateEnd.text = dateFormat.format(b.end)
        viewModel.selectBudget(b)
    }
}
