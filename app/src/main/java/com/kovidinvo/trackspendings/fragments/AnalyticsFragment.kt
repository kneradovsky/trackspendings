package com.kovidinvo.trackspendings.fragments


import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.DefaultAxisValueFormatter
import com.github.mikephil.charting.formatter.DefaultValueFormatter
import com.github.mikephil.charting.formatter.IValueFormatter
import com.github.mikephil.charting.utils.MPPointF
import com.github.mikephil.charting.utils.ViewPortHandler
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.data.PeriodType
import com.kovidinvo.trackspendings.data.SpendAnalytics
import com.kovidinvo.trackspendings.data.SpendTypeData
import com.kovidinvo.trackspendings.viewmodels.SpendAnalyticsViewModel
import com.kovidinvo.trackspendings.viewmodels.StatsData
import com.kovidinvo.trackspendings.viewmodels.StatsSpendType
import kotlinx.android.synthetic.main.fragment_analytics.*
import org.joda.time.DateTime
import java.io.File
import java.io.InputStreamReader


/**
 * A simple [Fragment] subclass.
 *
 */
class AnalyticsFragment : Fragment() {

    private lateinit var viewModel : SpendAnalyticsViewModel
    private val CHARTFILENAME = "spend_analytics.json"
    private var prevPressed : ImageButton? = null
    private var lastDate = DateTime.now()
    private val dateFormat = "dd.MM.YY"
    private val totalAmountFormat = "%,.2f"

    private val arrayTypeKeys = SpendAnalyticsViewModel.createEmptyStats().keys


    val mediumTypeFace by lazy { ResourcesCompat.getFont(context!!,R.font.roboto_medium) }
    val regularTypeFace by lazy { ResourcesCompat.getFont(context!!,R.font.roboto_light) }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_analytics, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SpendAnalyticsViewModel::class.java)
        viewModel.lcowner = this
        newPressedState(btnChartMonth)
        selectPeriod(PeriodType.MONTH)
        btnChartDay.setOnClickListener { btn -> newPressedState(btn as ImageButton);selectPeriod(PeriodType.DAY)  }
        btnChartMonth.setOnClickListener { btn -> newPressedState(btn as ImageButton);selectPeriod(PeriodType.MONTH) }
        btnChartWeek.setOnClickListener { btn -> newPressedState(btn as ImageButton);selectPeriod(PeriodType.WEEK)  }
        btnPeriodNext.setOnClickListener { btn ->  switchToNextPeriod()}
        btnPeriodPrev.setOnClickListener { btn -> switchToPrevPeriod() }
        initializeBarChart()
        initializePieChart()
        viewModel.periodTypeHg.observe(this, Observer { data -> updatePieData(data) })
        viewModel.periodStats.observe(this, Observer { data ->  updateBarData(data)})
    }


    private fun newPressedState(btn:ImageButton) {
        prevPressed?.isSelected=false
        btn.isSelected=true
        prevPressed=btn
        lastDate = DateTime.now()
    }

    private fun selectPeriod(pt: PeriodType,dateStart:DateTime = DateTime.now()) {
        showPeriodLabels(pt,dateStart)
        viewModel.connectPeriodSpends(pt, dateStart)
    }

    private fun readChartSettings() : List<SpendAnalytics> {
        val datafile = File(context!!.filesDir,CHARTFILENAME)
        val stream = if(datafile.exists()) datafile.inputStream() else context!!.assets.open(CHARTFILENAME)
        val data = Gson().fromJson<List<SpendAnalytics>>(InputStreamReader(stream), (object : TypeToken<List<SpendAnalytics>>(){}).type )
        return data
    }

    fun initializeBarChart() {
        val chart = anChBar
        chart.description.isEnabled = false

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        chart.setMaxVisibleValueCount(40)

        // scaling can now only be done on x- and y-axis separately
        chart.setPinchZoom(false)

        chart.setDrawGridBackground(false)
        chart.setDrawBarShadow(false)

        chart.setDrawValueAboveBar(false)
        chart.isHighlightFullBarEnabled = false

        // change the position of the y-labels
        val leftAxis = chart.axisLeft
        leftAxis.valueFormatter = DefaultAxisValueFormatter(2)
        leftAxis.axisMinimum = 0f // this replaces setStartAtZero(true)
        chart.axisRight.isEnabled = false

        val xLabels = chart.xAxis
        xLabels.position = XAxisPosition.TOP

        val l = chart.legend
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM)
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT)
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL)
        l.setDrawInside(false)
        l.setWordWrapEnabled(true)
        l.formSize = 8f
        l.formToTextSpace = 4f
        l.xEntrySpace = 6f


    }

    fun initializePieChart() {
        val chart = anChPie
        chart.setUsePercentValues(true)
        chart.description.setEnabled(false)

        chart.dragDecelerationFrictionCoef = .95f
        chart.setCenterTextTypeface(mediumTypeFace)
        chart.centerText = getString(R.string.diag_spending_type)+", %"

        chart.isDrawHoleEnabled = true
        chart.setHoleColor(Color.WHITE)

        chart.setTransparentCircleColor(Color.WHITE)
        chart.setTransparentCircleAlpha(110)

        chart.holeRadius = 58f
        chart.transparentCircleRadius = 61f

        chart.setDrawCenterText(true)

        chart.rotationAngle = 0f
        // enable rotation of the chart by touch
        chart.isRotationEnabled = true
        chart.isHighlightPerTapEnabled = true

        //chart.animateY(1400, Easing.EaseInOutQuad)

        val l = chart.getLegend()
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.VERTICAL
        l.setDrawInside(false)
        l.xEntrySpace = 7f
        l.yEntrySpace = 0f
        l.yOffset = 0f

        // entry label styling
        chart.setEntryLabelColor(ResourcesCompat.getColor(context!!.resources,R.color.coolGray,null))
        chart.setEntryLabelTypeface(regularTypeFace)
        chart.setEntryLabelTextSize(12f)


    }

    fun updatePieData(data: StatsSpendType) {
        var colors = mutableListOf<Int>()
        var totalAmount = 0.0
        val pdata = data.map { entry ->
            colors.add(SpendTypeData.typeColors[entry.key]!!)
            totalAmount += entry.value
            PieEntry(entry.value.toFloat(), getString(SpendTypeData.resStrIds[entry.key]
                            ?: R.string.nametype_unknown),
                    ResourcesCompat.getDrawable(context!!.resources, SpendTypeData.resIds[entry.key]
                            ?: R.drawable.ic_type_unknown, null)
            )
        }.toList()
        val dataset = PieDataSet(pdata, "")
        dataset.colors = colors
        dataset.iconsOffset = MPPointF(0f, 40f)
        dataset.selectionShift = 5f
        val piedata = PieData(dataset)
        piedata.setValueFormatter(DefaultValueFormatter(2))
        piedata.setValueTextSize(11f)
        piedata.setValueTextColor(Color.WHITE)
        piedata.setValueTypeface(regularTypeFace)
        anChPie.data = piedata
        anChPie.invalidate()
        anChPie.animateY(1000, Easing.EaseInOutQuad)
        txtTotalAmount.text = String.format(getString(R.string.formatAnalyticsTotalAmount),totalAmount)
    }

    fun updateBarData(data:StatsData) {
        val entries = data.map {sd ->
            BarEntry(sd.key.toFloat(),sd.value.values.map{ it.toFloat()}.toFloatArray())
        }.toList()
        val ds = BarDataSet(entries,"")
        ds.valueFormatter = object : IValueFormatter {
            override fun getFormattedValue(value: Float, entry: Entry?, dataSetIndex: Int, viewPortHandler: ViewPortHandler?): String {
                if(value<1.0f) return ""
                return String.format("%,.0f",value)
            }
        }
        ds.colors = arrayTypeKeys.map { type -> SpendTypeData.typeColors[type] }.toList()
        ds.stackLabels = arrayTypeKeys.map { type -> getString(SpendTypeData.resStrIds[type]!!)}.toTypedArray()
        val piedata = BarData(ds)
        anChBar.data=piedata
        //xAxis
        //set XAxis
        val xlabels = anChBar.xAxis
        xlabels.labelCount=data.keys.size
        xlabels.axisMaximum=(data.keys.max()?.toFloat() ?: 0.0f)
        if(xlabels.labelCount>23)
            xlabels.granularity=2.0f
        else xlabels.granularity=1.0f
        anChBar.invalidate()
        anChBar.animateY(1000,Easing.EaseInOutQuad)

    }

    fun switchToNextPeriod() {
        if(lastDate.withTimeAtStartOfDay().millis>=DateTime.now().withTimeAtStartOfDay().millis) return
        val pt = when(prevPressed!!.id) {
            R.id.btnChartDay -> {
                lastDate = lastDate.plusDays(1)
                PeriodType.DAY
            }
            R.id.btnChartWeek -> {
                lastDate = lastDate.plusWeeks(1)
                PeriodType.WEEK
            }
            R.id.btnChartMonth -> {
                lastDate = lastDate.plusMonths(1)
                PeriodType.MONTH
            }
            else -> PeriodType.MONTH
        }
        selectPeriod(pt,lastDate)

    }



    fun switchToPrevPeriod() {
        val pt = when(prevPressed!!.id) {
            R.id.btnChartDay -> {
                lastDate = lastDate.minusDays(1)
                PeriodType.DAY
            }
            R.id.btnChartWeek -> {
                lastDate = lastDate.minusWeeks(1)
                PeriodType.WEEK
            }
            R.id.btnChartMonth -> {
                lastDate = lastDate.minusMonths(1)
                PeriodType.MONTH
            }
            else -> PeriodType.MONTH
        }
        selectPeriod(pt,lastDate)

    }

    fun showPeriodLabels(pt:PeriodType,startDate: DateTime,endDate: DateTime? = null) {
        when(pt) {
            PeriodType.DAY -> {
                txtPeriodEnd.text=""
                txtPeriodStart.text = startDate.toString(dateFormat)
            }
            PeriodType.WEEK -> {
                txtPeriodStart.text = startDate.withDayOfWeek(1).toString(dateFormat)
                txtPeriodEnd.text = startDate.withDayOfWeek(7).toString(dateFormat)
            }
            PeriodType.MONTH -> {
                txtPeriodStart.text = startDate.withDayOfMonth(1).toString(dateFormat)
                txtPeriodEnd.text = startDate.plusMonths(1).withDayOfMonth(1).minusDays(1).toString(dateFormat)
            }
        }
    }
}
