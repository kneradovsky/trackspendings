package com.kovidinvo.trackspendings.charts

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import androidx.core.content.res.ResourcesCompat
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.DefaultValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.SpendNotification
import com.kovidinvo.trackspendings.data.SpendTypeData

class SpendTypesDataChart: PieChart {
    val ctx:Context
    constructor(ctx1: Context) : super(ctx1) {ctx=ctx1}
    constructor(ctx1: Context, attr: AttributeSet) : super(ctx1,attr) {ctx=ctx1}
    constructor(ctx1: Context, attr: AttributeSet, defStyle:Int) : super(ctx1,attr,defStyle) {ctx=ctx1}

    fun initialize() {
        //super.init()
        setDrawCenterText(false)
        description.isEnabled=false
        centerText=""
        isDrawHoleEnabled=true
        setHoleColor(Color.TRANSPARENT)
        rotationAngle=0f;

        isRotationEnabled=false
        isHighlightPerTapEnabled=true
        legend.isEnabled=false
        val dataset = PieDataSet(emptyList(),"")
        dataset.colors = ColorTemplate.MATERIAL_COLORS.toList()
        val tdata = PieData(dataset)
        data = tdata
    }
    fun drawChart(indata:List<SpendNotification>) {
        val pdata = indata.map { sp ->
            PieEntry(sp.amount.toFloat(),
                    resources.getString(SpendTypeData.resStrIds[sp.type]?:R.string.nametype_unknown),
                    ResourcesCompat.getDrawable(ctx.resources, SpendTypeData.resIds[sp.type]?: R.drawable.ic_type_unknown,null)
            )
        }.toList()
        val dataset = PieDataSet(pdata,"")
        dataset.colors = ColorTemplate.MATERIAL_COLORS.toMutableList()
        val piedata = PieData(dataset)
        piedata.setValueTextSize(10.0f)
        piedata.setValueFormatter(DefaultValueFormatter(2))
        data = piedata
        invalidate()
    }
}