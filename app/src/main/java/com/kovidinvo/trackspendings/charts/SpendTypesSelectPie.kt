package com.kovidinvo.trackspendings.charts

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import androidx.core.content.res.ResourcesCompat
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.data.SpendTypeData

class SpendTypesSelectPie : PieChart {
    constructor(ctx1:Context) : super(ctx1) {ctx=ctx1}
    constructor(ctx1:Context,attr: AttributeSet) : super(ctx1,attr) {ctx=ctx1}
    constructor(ctx1:Context,attr: AttributeSet,defStyle:Int) : super(ctx1,attr,defStyle) {ctx=ctx1}

    val ctx:Context


    fun initialize() {
        //super.init()
        setDrawCenterText(false)
        description.isEnabled=false
        centerText=""
        isDrawHoleEnabled=true
        setHoleColor(Color.TRANSPARENT)
        rotationAngle=0f;

        isRotationEnabled=false
        isHighlightPerTapEnabled=true
        legend.isEnabled=false
        val entries = SpendTypeData.resIds.entries
                .map{ p -> PieEntry(10f,ResourcesCompat.getDrawable(ctx.resources,p.value,null),p.key)}
                .toList()
        val dataset = PieDataSet(entries,"")
        dataset.colors = ColorTemplate.MATERIAL_COLORS.toList()
        val tdata = PieData(dataset)
        data = tdata
    }
}