package com.kovidinvo.trackspendings

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.google.gson.JsonElement
import com.kovidinvo.trackspendings.data.MCCodesData
import com.yandex.mapkit.map.PlacemarkMapObject
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.json.JSONObject
import java.util.*
import java.util.concurrent.Executors


class DateConverter {

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? = if (value != null) Date(value) else null

    @TypeConverter
    fun toTimestamp(value: Date?): Long? = value?.time ?: 0

}
class StringArrayConverter {
    @TypeConverter
    fun fromArray(value: MutableList<String>): String = value.joinToString(",")

    @TypeConverter
    fun toArray(value: String?) : MutableList<String> = value?.split(",")?.toMutableList() ?: emptyList<String>().toMutableList()
}

@Entity(tableName = "spendings")
@TypeConverters(DateConverter::class)
data class SpendNotification(
        @PrimaryKey
        val spid: String = UUID.randomUUID().toString(),
        val bank: String, var card: Int,
        val pos: String, val date: Date, val amount: Double,
        var lattitude: Double? = null, var longitude: Double? = null,
        var mccode: Int? = null,
        var type: String = "unknown"
) {
    @Ignore
    var mark: PlacemarkMapObject? = null

    fun toJSONObject() : JSONObject {
        val res = JSONObject()
        res.put("spid",spid)
        res.put("bank",bank)
        res.put("card",card)
        res.put("pos",pos)
        res.put("date",date.time)
        res.put("amount",amount)
        res.put("lattitude",lattitude)
        res.put("longitude",longitude)
        res.put("type",type)
        return res
    }
}


@Entity(tableName = "cards")
data class PayCard(
        @PrimaryKey
        var pcid: String = UUID.randomUUID().toString(),
        val type: String = "", val num: Int,
        val bank: String, val balance: Double = 0.0,
        val name: String = String.format("%04d", num)
)

@Entity(tableName = "mcc")
data class MCCode(
        @PrimaryKey
        val code: Int = 0,
        val name: String,
        val desc: String
) {
    companion object {
        fun fromJson(js: JsonElement): MCCode {
            val jsobj = js.asJsonObject
            return MCCode(code = jsobj["mcc"].asInt, name = jsobj["irs_description"].asString, desc = jsobj["edited_description"].asString)
        }
    }
}

@Entity(tableName = "poses")
data class POS(
        @PrimaryKey
        val name: String = "unknown_pos",
        val type: String,
        var lattitude: Double? = null, var longitude: Double? = null
)


@Entity(tableName = "budgets")
@TypeConverters(DateConverter::class,StringArrayConverter::class)
data class Budget(
        @PrimaryKey
        val bgid: String = UUID.randomUUID().toString(),
        val name : String,
        val start: Date,
        val end:Date,
        val periodtype : String,
        var active : Int,
        val type: MutableList<String>,
        var planned: Double,
        var actual: Double
)

data class TypeAmountTuple(
        @ColumnInfo(name = "type") var type: String,
        @ColumnInfo(name = "amount") var amount: Double
)

@Dao
interface BudgetDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(bg:Budget)
    @Delete
    fun delete(bg:Budget)
    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(bg:Budget)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(bgs: List<Budget>)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateMany(bgs: List<Budget>)

    @Query("select * from budgets")
    fun getAll() : LiveData<List<Budget>>

    @Query("select * from budgets where active=1")
    fun getAllActive() : Observable<List<Budget>>

    @Query("select * from budgets where active=1")
    fun getAllActiveLive() : LiveData<List<Budget>>


    @Query("select * from budgets where active=0 order by 'end' desc")
    fun getAllInactiveLive() : LiveData<List<Budget>>

    @Query("select * from budgets where name=:name")
    fun getBudgetByName(name:String) : Maybe<Budget>

    @Query("select * from budgets where periodtype=:period and active=1")
    fun getActiveBudgetsByPeriod(period:String) : LiveData<List<Budget>>


    @Query("update budgets set actual=actual+:amount where type LIKE :type and active=1")
    fun updateActiveBudgets(amount:Double,type:String) : Int

    @Query("select * from spendings where date between :start and :end and type in (:types) order by date DESC")
    fun getAllSpendingsInBudget(start:Long,end:Long,types:List<String>) : List<SpendNotification>

    @Query("select * from budgets where active=1 and type LIKE :type")
    fun getActiveBudgetsByType(type:String) : List<Budget>
}

@Dao
interface CardsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(pc: PayCard)

    @Delete
    fun delete(pc: PayCard)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(pc: PayCard)

    //queries
    @Query("Select * from cards where pcid=:pcid")
    fun cardById(pcid: String): Observable<PayCard>

    @Query("select * from cards")
    fun getAll(): LiveData<List<PayCard>>

    @Query("Select * from cards where bank=:bank")
    fun cardsByBank(bank: String): LiveData<List<PayCard>>

    @Query("Select * from cards where bank=:bank and num=:num")
    fun cardByNum(bank: String, num: Int): Maybe<PayCard>

    @Query("select distinct bank from cards")
    fun allBanks(): LiveData<List<String>>
}

@Dao
interface MCCDao {
    //mcccodes
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMCC(mc: MCCode)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllMCC(mcs: Array<MCCode>)

    @Query("select * from mcc")
    fun getAllMCC(): Flowable<List<MCCode>>

    @Query("select * from mcc where code=:code")
    fun getMCC(code: Int): Maybe<MCCode>


}

@Dao
interface SpendingsDao {
    @Query("Select * from spendings")
    fun getAll(): Observable<List<SpendNotification>>

    @Query("Select * from spendings order by date desc limit :rows")
    fun getLastRows(rows: Int): List<SpendNotification>

    @Query("select * from spendings where spid=:spid")
    fun getById(spid:String) : SpendNotification?


    @Query("Select * from spendings where bank=:bank")
    fun findBySender(bank: String): List<SpendNotification>

    @Query("Select * from spendings where mccode=:mcc")
    fun findByMCC(mcc: Int): List<SpendNotification>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(sp: SpendNotification)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(sps:List<SpendNotification>)

    @Delete
    fun delete(sp: SpendNotification)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(sp: SpendNotification)

    @Query("select distinct bank from spendings")
    fun allBanks(): LiveData<List<String>>

    @Query("select * from spendings where date between :start and :end ORDER BY date desc")
    fun getSpendingsByDates(start: Long, end: Long): Single<List<SpendNotification>>

    @Query("select * from spendings where date>:start")
    fun getSpendingsFromDate(start:Long) : LiveData<List<SpendNotification>>

    @Query("select * from spendings where date between :start and :end ORDER BY date desc")
    fun getSpendingsByDatesLive(start: Long, end: Long): LiveData<List<SpendNotification>>


    @Query("update spendings set type=:type where pos=:pos")
    fun updateAllSpendingTypeInPos(pos: String, type: String): Int

    @Query("select * from spendings where type=:type and pos=:pos")
    fun getSpendingsByTypeAndPos(pos: String, type: String): Observable<List<SpendNotification>>

    @Query("select type,sum(amount) as amount from spendings where card not in (:cards) group by type")
    fun getSpendingsHistogramByNotCards(cards:List<Int>) : List<TypeAmountTuple>
}


@Dao
interface PosDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPos(pos: POS)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updatePos(pos: POS)

    @Query("select * from poses where name=:name")
    fun getPOSByName(name: String): Maybe<POS>

    @Query("select * from poses where type=:type")
    fun getPOSByType(type: String): Observable<List<POS>>

}

@Database(entities = arrayOf(SpendNotification::class, PayCard::class, MCCode::class, POS::class,Budget::class)
        , version = 7, exportSchema = false)
abstract class SpendingsDatabase : RoomDatabase() {
    abstract fun spendingsDao(): SpendingsDao
    abstract fun cardsDao(): CardsDao
    abstract fun mccDao(): MCCDao
    abstract fun posDao(): PosDao
    abstract fun budgetDao() : BudgetDao

    companion object {


        @Volatile
        private var INSTANCE: SpendingsDatabase? = null

        fun getInstance(ctx: Context): SpendingsDatabase {
            val db =
                    INSTANCE ?: synchronized(this) {
                        INSTANCE ?: buildDatabase(ctx).also { INSTANCE = it }
                    }
            return db
        }

        private fun buildDatabase(ctx: Context): SpendingsDatabase {
            val db = Room.databaseBuilder(ctx.applicationContext, SpendingsDatabase::class.java, "spendings.db")
                    .addMigrations(migration1_2, migration2_3, migration3_4, migration4_5, migration5_6,migration6_7)
                    .build()
            return db
        }

        private val migration1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("create table cards (pcid TEXT not null,'type' TEXT not null,'num' INTEGER not null, bank TEXT not null,balance REAL not null,PRIMARY KEY(pcid))")
                database.execSQL("create table mcc(code INTEGER not null,'name' TEXT not null,'desc' TEXT not null, primary key(code))")
                database.execSQL("alter table spendings add column card INTEGER not null default 0")
                database.execSQL("alter table spendings add column mccode INTEGER default 0")

            }

        }
        private val migration2_3 = object : Migration(2, 3) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("alter table spendings add column 'type' TEXT not null default 'unknown'")
            }
        }
        private val migration3_4 = object : Migration(3, 4) {
            override fun migrate(database: SupportSQLiteDatabase) {
                Log.d("Migration 3_4", "fantom migration")
            }
        }
        private val migration4_5 = object : Migration(4, 5) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("alter table cards add column name TEXT not null default '0000'")
                database.execSQL("update cards set name=printf('%04d',num)")
            }
        }
        private val migration5_6 = object : Migration(5, 6) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("create table poses (name TEXT not null,'type' TEXT not null,lattitude REAL,longitude REAL,primary key(name))")
            }
        }
        private val migration6_7 = object : Migration(6, 7) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("create table budgets(bgid TEXT not null,'name' TEXT not null, 'type' TEXT not null,periodtype TEXT not null,planned REAL not null,actual REAL not null, 'start' INTEGER not null,'end' INTEGER not null,active INTEGER not null,primary key(bgid))")
            }
        }

    }

    fun updateAllActiveBudgets() {
        budgetDao().getAllActive()
                .observeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
                .subscribe { budgets ->
                    budgets.forEach { budget ->
                        val currentSpendings = getBudgetCurrentSpendings(budget)
                        budget.actual=currentSpendings
                        budgetDao().update(budget)
                    }
                } != null

    }

    fun getBudgetCurrentSpendings(budget:Budget) : Double {
        val startSpends = budget.start.time
        val endSpends = budget.end.time
        val catstr = "("+budget.type.map {s -> "'$s'"}.joinToString(",")+")"
        val cur=query("select sum(amount) from spendings where date between ? and ? and type in $catstr",arrayOf(startSpends,endSpends))
        var amount = 0.0
        if(cur.count>0) {
            cur.moveToFirst()
            amount = cur.getDouble(0)
        }
        cur.close()
        return amount
    }
}