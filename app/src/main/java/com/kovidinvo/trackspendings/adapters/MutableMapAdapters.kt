package com.kovidinvo.trackspendings.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.viewmodels.SpeedTypesViewModel
import kotlinx.android.synthetic.main.item_image_checkbox.view.*
import kotlinx.android.synthetic.main.item_image_radiobutton.view.*

abstract class MutableMapRecyclerAdapter(val data: List<MutableMap<String,Any>>,val layout : Int) : RecyclerView.Adapter<MutableMapRecyclerAdapter.ViewHolder>() {
    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(layout,parent,false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }
}

class BudgetCategoriesListAdapter(dat:List<MutableMap<String,Any>>) : MutableMapRecyclerAdapter(dat, R.layout.item_image_checkbox) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.icon.setImageResource(data[position]["image"] as Int)
        holder.view.checkedText.text = data[position]["text"].toString()
        holder.view.checkbox.setOnCheckedChangeListener { compoundButton, b -> data[position]["checked"] = b }
        holder.view.checkbox.isChecked = data[position]["checked"] as Boolean
    }

}

class AllSpendingTypeListAdapter(dat:List<MutableMap<String,Any>>) : MutableMapRecyclerAdapter(dat,R.layout.item_image_radiobutton) {
    var lastSelectedType :String? = null

    var lastSelectedView : CompoundButton? = null

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.radiobutton.setOnCheckedChangeListener {buttonView,isChecked ->
            if(!isChecked) return@setOnCheckedChangeListener
            lastSelectedView?.isChecked=false
            lastSelectedType = data[position]["id"].toString()
            lastSelectedView=buttonView
        }

        holder.view.selectedIcon.setImageResource(data[position]["image"] as Int)
        holder.view.radiobutton.isChecked = (lastSelectedType ?: "") == data[position]["id"].toString()
        holder.view.selectedText.text = data[position]["text"].toString()

    }


}

class SpeedTypesListAdapter(dat:List<MutableMap<String,Any>>,val model: SpeedTypesViewModel) : MutableMapRecyclerAdapter(dat, R.layout.item_image_checkbox) {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.view.checkbox.setOnCheckedChangeListener { compoundButton, b ->
            if(data[position]["checked"]==b) return@setOnCheckedChangeListener

            if(model.selectItem(data[position]["id"].toString(),b))
                data[position]["checked"] = b
            else {
                Snackbar.make(holder.view,R.string.maximum_speedtypes_already_selected,Snackbar.LENGTH_SHORT).show()
                compoundButton.isChecked=false
            }
        }
        holder.view.icon.setImageResource(data[position]["image"] as Int)
        holder.view.checkbox.isChecked = data[position]["checked"] as Boolean
        holder.view.checkedText.text = data[position]["text"].toString()
    }

}