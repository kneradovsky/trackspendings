package com.kovidinvo.trackspendings.adapters

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.kovidinvo.trackspendings.R


class SwipeToDeleteHandler(val ctx: Context, private val onDelete : (RecyclerView.ViewHolder) -> Unit) : ItemTouchHelper.SimpleCallback(0,ItemTouchHelper.LEFT) {
    var pinnedTypes : List<Int>? = null

    private val backgound = ColorDrawable(Color.RED)
    private val delMark = ContextCompat.getDrawable(ctx, R.drawable.ic_delete)?.apply {
        setColorFilter(Color.WHITE,PorterDuff.Mode.SRC_ATOP)
    }
    private val delMarkMargin = ctx.resources.getDimension(R.dimen.ic_delete_margin).toInt()


    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean = false

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val itemType = viewHolder.itemViewType
        if(pinnedTypes?.contains(itemType) ?: false) return

        onDelete(viewHolder)
    }

    override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
        if(viewHolder.adapterPosition<0) return
        var ldX = dX
        var ldY=dY
        if(!(pinnedTypes?.contains(viewHolder.itemViewType) ?: false)) {
            val view = viewHolder.itemView
            backgound.apply {
                setBounds(view.right + dX.toInt(), view.top, view.right, view.bottom)
                draw(c)
            }
            delMark?.apply {
                val y = view.top + (view.bottom - view.top - delMark.intrinsicHeight) / 2
                setBounds(view.right - delMarkMargin - delMark.intrinsicWidth, y, view.right - delMarkMargin, y + delMark.intrinsicHeight)
                draw(c)
            }
        } else { ldX=0.0f;ldY=0.0f }
        super.onChildDraw(c, recyclerView, viewHolder, ldX, ldY, actionState, isCurrentlyActive)
    }
}