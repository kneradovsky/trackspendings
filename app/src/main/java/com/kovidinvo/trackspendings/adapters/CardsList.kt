package com.kovidinvo.trackspendings.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.kovidinvo.trackspendings.PayCard
import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.data.Bank
import com.kovidinvo.trackspendings.data.BankData
import kotlinx.android.synthetic.main.item_card.view.*

class CardsList(private var data:List<PayCard>,private val m_Presenter: CardListPresenter? = null) : RecyclerView.Adapter<CardsList.ViewHolder>() {
    private var selectedItemView : View? = null
    private val TAG="CardList Adapter"
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_card,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pc = data[position]
        holder.pc = pc
        holder.itemView.pc_num.text = pc.name
        holder.itemView.pc_balance.text = holder.itemView.context.getString(R.string.balanceHideStr)
        val bankresid = BankData.logosList[pc.bank] ?: R.drawable.ic_banklogo_unknown
        if(bankresid==R.drawable.ic_banklogo_unknown)
            Log.w(TAG,"bank ${pc.bank} is found in database, but not supported")
        holder.itemView.pc_bank.setImageResource(bankresid)


        holder.itemView.setOnClickListener {
            selectedItemView?.isSelected=false
            it.isSelected = true
            selectedItemView = it
        }
        holder.itemView.pc_balance.setOnClickListener {
            val balView = it as TextView
            if(holder.balanceHidden) {
                balView.text =  String.format("%,.2f р.",holder.pc.balance)
            } else balView.text = balView.context.getString(R.string.balanceHideStr)
            holder.balanceHidden=!holder.balanceHidden
        }

        holder.itemView.btnStatChange.setOnClickListener {
            it.isSelected=!it.isSelected
            m_Presenter?.onCardStatChange(pc.num,it.isSelected)
        }
        holder.itemView.btnStatChange.isSelected=true
    }

    fun replaceData(indata:List<PayCard>) {
        data=indata
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var balanceHidden = true
        lateinit var pc : PayCard



    }


}

interface CardListPresenter {
    fun onCardStatChange(num:Int,state:Boolean)
}