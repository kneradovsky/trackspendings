package com.kovidinvo.trackspendings.adapters

import android.graphics.drawable.ClipDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.children
import androidx.recyclerview.widget.RecyclerView
import com.kovidinvo.trackspendings.Budget
import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.data.SpendTypeData
import kotlinx.android.synthetic.main.item_budget.view.*
import kotlinx.android.synthetic.main.item_budget_list_splitter.view.*
import java.text.SimpleDateFormat
import java.util.*

class BudgetsListAdapter(private var activeData: List<Budget>,private var pastData: List<Budget>,private val mBudgetPresenter : BudgetPresenter) : RecyclerView.Adapter<BudgetsListAdapter.ViewHolder>() {

    private val TAG="BudgetListAdapter"
    private var selectedItemView : View? = null
    private val dateFormat = SimpleDateFormat("dd.MM.yyyy")

    var dataActive : List<Budget> get() = activeData
        set(value) {
            activeData=value
            notifyDataSetChanged()
        }
    var dataPast : List<Budget> get() = pastData
    set(value) {
        pastData=value
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = when(viewType) {
            1,2 -> LayoutInflater.from(parent.context).inflate(R.layout.item_budget_list_splitter,parent,false)
            3,4 -> LayoutInflater.from(parent.context).inflate(R.layout.item_budget,parent,false)
            else -> null
        }
        return ViewHolder(view!!,viewType)
    }

    override fun getItemCount(): Int {
        val size=activeData.size+pastData.size+2 //active list + passive list + 2 delimiters
        return size
    }

    override fun getItemViewType(position: Int): Int {
        if(position==0) return 1
        if(position>0 && activeData.size>0 && position<(activeData.size+1)) return 3
        if(position==(activeData.size+1)) return 2
        return 4
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val budget = when(holder.type) {
            3 -> activeData[position-1] //active data position
            4 -> pastData[position-activeData.size-2] //past data position
            1 -> {holder.item.itemName.text = holder.itemView.resources.getString(R.string.active_budgets) ;null}
            2 -> {holder.item.itemName.text = holder.itemView.resources.getString(R.string.past_budgets);null}
            else -> null
        }
        if(budget!=null) {
            val level = (budget.actual/budget.planned*10000).toInt()
            (holder.item.budgetProgress.drawable as? ClipDrawable)?.setLevel(level)
            holder.item.budgetName.text=budget.name
            holder.item.budgetActual.text=formatAmount(budget.actual)
            holder.item.budgetPlanned.text=formatAmount(budget.planned)
            holder.item.budgetListTypes.removeAllViews()


            budget.type.onEach { type->
                val imgResId=SpendTypeData.resIds[type] ?: R.drawable.ic_type_unknown
                val imgView = ImageView(holder.item.context)
                imgView.setImageResource(imgResId)
                holder.item.budgetListTypes.addView(imgView)
            }
            holder.item.setOnClickListener {
                Log.i(TAG,"budget clicked")
                selectedItemView?.isSelected=false
                it.isSelected = true
                selectedItemView = it
                mBudgetPresenter.onBudgetClick(budget)
            }
        }
    }

    fun formatAmount(am:Double) : String = String.format("%,.2f",am)




    class ViewHolder(val item: View,val type:Int) : RecyclerView.ViewHolder(item){

    }
}

interface BudgetPresenter {
    fun onBudgetClick(b:Budget)
}