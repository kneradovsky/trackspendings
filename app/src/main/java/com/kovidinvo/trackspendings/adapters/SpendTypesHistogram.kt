package com.kovidinvo.trackspendings.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.SpendNotification
import com.kovidinvo.trackspendings.data.SpendTypeData
import kotlinx.android.synthetic.main.item_spendtypelist.view.*


class SpendTypesHistogram(var data : List<SpendNotification>) : RecyclerView.Adapter<SpendTypesHistogram.ViewHolder>() {
    private var selectedItemView : View? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_spendtypelist,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sp = data[position]
        holder.itemView.sp_type.text = holder.itemView.resources.getString(SpendTypeData.resStrIds[sp.type]?:R.string.nametype_unknown)
        holder.itemView.sp_amount.text = String.format("%,.2f р.",sp.amount)
        val context = holder.itemView.context
        val resid = SpendTypeData.resIds[sp.type]?:R.drawable.ic_type_unknown
        holder.itemView.imgSpendType.setImageResource(resid)

        holder.itemView.setOnClickListener {
            selectedItemView?.isSelected=false
            it.isSelected = true
            selectedItemView = it
        }
     }

    fun replaceData(nd: List<SpendNotification>) {
        data=nd
        notifyDataSetChanged()
    }

    class ViewHolder(item: View) : RecyclerView.ViewHolder(item) {
    }
}