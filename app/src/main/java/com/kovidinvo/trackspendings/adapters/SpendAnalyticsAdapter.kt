package com.kovidinvo.trackspendings.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.LegendEntry
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.DefaultValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.data.*
import com.kovidinvo.trackspendings.viewmodels.SpendAnalyticsViewModel
import kotlinx.android.synthetic.main.item_analytics_chart.view.*

class SpendAnalyticsAdapter(var data: List<SpendAnalytics>, val viewmodel: SpendAnalyticsViewModel) : RecyclerView.Adapter<SpendAnalyticsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_analytics_chart, parent, false)
        return ViewHolder(v, viewmodel)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.data = data[position]
        holder.init()
    }

    class ViewHolder(val view: View, val viewmodel: SpendAnalyticsViewModel) : RecyclerView.ViewHolder(view) {
        val arrayTypeKeys = SpendAnalyticsViewModel.createEmptyStats().keys
        lateinit var data: SpendAnalytics
        fun init() {
            val chart = when (data.chartType) {
                SpendChartType.BARSTACK.ct -> createStackBarChart()
                SpendChartType.PIE.ct -> createPieChart()
                else -> return
            }
            chart.minimumHeight = DrawableImageProvider.dp2px(200.0f).toInt()
            view.phChart.addView(chart)
        }

        fun createStackBarChart(): BarChart {
            val chart = BarChart(view.context)
            chart.legend.isEnabled = true
            chart.description.isEnabled=true
            val liveData = when(data.periodType) {
                PeriodType.DAY.pt -> {
                    chart.description.text = view.context.getString(R.string.periodDay)
                    viewmodel.dailyStats
                }
                PeriodType.WEEK.pt -> {
                    chart.description.text = view.context.getString(R.string.periodWeek)
                    viewmodel.weeklyStats
                }
                PeriodType.MONTH.pt -> {
                    chart.description.text = view.context.getString(R.string.periodMonth)
                    viewmodel.monthlyStats
                }
                else -> null
            }
            liveData?.observe(viewmodel.lcowner, Observer { statsdata ->
                val colors= mutableListOf<Int>()
                val labels = mutableListOf<String>()
                val entries = statsdata.entries.map {sd ->
                    BarEntry(sd.key.toFloat(),sd.value.values.map{ it.toFloat()}.toFloatArray())
                }.toList()
                val ds = BarDataSet(entries,"")
                ds.colors = arrayTypeKeys.map { type -> SpendTypeData.typeColors[type] }.toList()
                ds.stackLabels = arrayTypeKeys.map { type -> view.context.getString(SpendTypeData.resStrIds[type]!!)}.toTypedArray()
                val data = BarData(ds)
                chart.data=data

                chart.invalidate()
            })
            return chart
        }

        fun createPieChart(): PieChart {
            val chart = PieChart(view.context)
            chart.description.text = view.context.getString(R.string.lblTypesHistogram)
            viewmodel.typeHistogram.observe(viewmodel.lcowner, Observer { th ->
                var colors = mutableListOf<Int>()
                val pdata = th.entries.map { entry ->
                    colors.add(SpendTypeData.typeColors[entry.key]!!)
                    PieEntry(entry.value.toFloat(),
                            view.context.getString(SpendTypeData.resStrIds[entry.key]
                                    ?: R.string.nametype_unknown),
                            ResourcesCompat.getDrawable(view.context.resources, SpendTypeData.resIds[entry.key]
                                    ?: R.drawable.ic_type_unknown, null)
                    )
                }.toList()
                val dataset = PieDataSet(pdata, "")
                dataset.colors = colors
                val piedata = PieData(dataset)
                piedata.setValueTextSize(10.0f)
                piedata.setValueFormatter(DefaultValueFormatter(2))
                chart.data = piedata
                chart.invalidate()
            })
            return chart
        }
    }
}