package com.kovidinvo.trackspendings.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SimpleAdapter
import androidx.appcompat.widget.ListPopupWindow
import androidx.recyclerview.widget.RecyclerView
import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.SpendNotification
import com.kovidinvo.trackspendings.data.*
import com.kovidinvo.trackspendings.fragments.SpendingsListPresenter

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_spending.view.*
import java.text.SimpleDateFormat
import java.util.*


class SpendingsAdapter(val data: MutableList<SpendNotification>,ds: Observable<SpendNotification>?, private val mPresenter: SpendingsListPresenter,private var emptyLayout : Int? = null) : RecyclerView.Adapter<SpendingsAdapter.DefaultViewHolder>() {
    private val TAG = "SpendingsAdapter"
    private val df_old = SimpleDateFormat("yyyy-MM-dd HH:mm")
    private val df_today = SimpleDateFormat("HH:mm")
    private var selectedItemView : View? = null
    private lateinit var selTypeAdaper : SimpleAdapter
    private lateinit var typeListActivityItems : List<Map<String,Any>>


    init {
        if(ds!=null) {
            ds.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { notif ->
                        var ind = data.indexOf(notif)
                        if (ind == -1) {
                            ind = 0
                            val prevSize = data.size
                            data.add(ind, notif)
                            if(prevSize==0) notifyItemChanged(0)
                            else notifyItemInserted(ind)
                            mPresenter.scrollToPosition(ind)
                        } else notifyItemChanged(ind)
                    } != null
        }
        if(emptyLayout==null) emptyLayout = R.layout.item_spendings_empty
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DefaultViewHolder {

        //(parent as RecyclerView).scrollToPosition(0)
        if(viewType==2) { //empty view
            val v = LayoutInflater.from(parent.context).inflate(emptyLayout!!,parent,false)
            return DefaultViewHolder(v)
        }
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_spending,parent,false)
        if(!::selTypeAdaper.isInitialized) {
            //read filter values
            val types = mPresenter.getPreferredTypes()
            val sourceTypes = if(types.isEmpty()) SpendingType.values().take(10).toTypedArray() else SpendingType.values().filter { t -> types.contains(t.n) }.toTypedArray()
            typeListActivityItems = sourceTypes
                    .map {v -> mapOf("id" to v.n,"text" to  parent.resources.getString(SpendTypeData.resStrIds[v.n]!!),"image" to SpendTypeData.resIds[v.n]!!)}
                    .plus(mapOf("id" to "showmore","text" to parent.context.getString(R.string.showMoreTypes),"image" to R.drawable.ic_show_more))
            selTypeAdaper = SimpleAdapter(parent.context, typeListActivityItems, android.R.layout.activity_list_item,
                    arrayOf("text", "image"), arrayOf(android.R.id.text1, android.R.id.icon).toIntArray())
        }
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
        return if(data.size==0) 1 else data.size
    }

    override fun getItemViewType(position: Int): Int {
        if(data.size==0 && position==0) return 2
        return 1
    }

    override fun onBindViewHolder(vh: DefaultViewHolder, position: Int) {
        //val binding : SpendingItemBinding? = DataBindingUtil.getBinding<SpendingItemBinding?>(holder.itemView!!)
        //binding?.sp=data[position]
        if(vh.isEmpty) return
        val holder = vh as ViewHolder
        val sp = data[position]
        holder.itemView.isSelected=false
        holder.itemView.sp_date.text = formatDate(sp.date)
        holder.itemView.sp_pos.text = sp.pos
        holder.itemView.sp_amount.text = formatAmount(sp.amount)
        holder.itemView.sp_card.text = if(sp.card>0) String.format("%04d",sp.card) else "0000"
        holder.itemView.setOnClickListener {
            selectedItemView?.isSelected=false
            it.isSelected = true
            selectedItemView = it
            mPresenter.onListItemClick(sp)
        }
        val context = holder.itemView.context
        var resid = SpendTypeData.resIds[sp.type]
        if(resid==null) resid = R.drawable.ic_type_unknown
        holder.itemView.imgSpendType.setImageResource(resid)
        holder.popupList.anchorView=holder.itemView.imgSpendType
        if(mPresenter.isTypeImageClickSupported) {
            holder.itemView.imgSpendType.setOnClickListener {
                holder.popupList.setAdapter(selTypeAdaper)
                holder.popupList.setOnItemClickListener {parent,view,pos,id->
                    holder.popupList.dismiss()
                    val newtype = typeListActivityItems[pos]["id"]?.toString() ?: SpendingType.UNKNOWN.n
                    if(newtype=="showmore") {
                        mPresenter.selectFromAllTypes(sp)
                        holder.popupList.dismiss()
                        return@setOnItemClickListener
                    }
                    sp.type=newtype
                    mPresenter.onTypeImageClick(sp)
                }
                holder.popupList.show()
            }
        }
        val bankresid = BankData.logosList[sp.bank] ?: R.drawable.ic_banklogo_unknown
        if(bankresid==R.drawable.ic_banklogo_unknown)
            Log.w(TAG,"bank ${sp.bank} is found in database, but not supported")
        holder.itemView.sp_bank.setImageResource(bankresid)
    }

    fun formatAmount(am:Double) : String {
        return String.format("%,.2f",am)
    }

    fun formatDate(d: Date) : String{
        var now = Date().time
        now -= now % (86400*1000)
        if((now - d.time)>0) //old date
            return df_old.format(d)
        else return df_today.format(d)
    }

    fun addAllItems(newdata: List<SpendNotification>) {
        data.addAll(newdata)
        notifyDataSetChanged()
    }


    class ViewHolder(itemView:View) : DefaultViewHolder(itemView) {
        override val isEmpty=false
        val popupList = ListPopupWindow(itemView.context)
        init {
            popupList.anchorView =itemView
            popupList.isModal=true
            //popupList.height=(DrawableImageProvider.dp2px(27.0f)*SpendingType.values().size).toInt()
            popupList.width=DrawableImageProvider.dp2px(16.0f+10.0f*18).toInt()

        }

    }

    open class DefaultViewHolder(itemView:View) : RecyclerView.ViewHolder(itemView) {
        open val isEmpty=true
    }

}

