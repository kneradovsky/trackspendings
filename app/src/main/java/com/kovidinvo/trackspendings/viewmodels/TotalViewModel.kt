package com.kovidinvo.trackspendings.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.kovidinvo.trackspendings.App
import com.kovidinvo.trackspendings.SpendNotification

class TotalViewModel(application:Application) : AndroidViewModel(application) {
    val app = application as App
    var total : MutableLiveData<Double> = MutableLiveData()
    var typeHistogram : MutableLiveData<List<SpendNotification>> = MutableLiveData()
    init {
        total.postValue(0.0)
        typeHistogram.postValue(emptyList())
    }



}
