package com.kovidinvo.trackspendings.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.kovidinvo.trackspendings.R
import com.kovidinvo.trackspendings.fragments.PeriodFilterFragment
import kotlinx.android.synthetic.main.fragment_period_filter.*
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

interface onDividerClickedListener {
    fun onDividerClick()
}

class PeriodFilterViewModel(val app:Application) : AndroidViewModel(app) {
    var periodSelection : Int = R.id.rb_day
    var isShowNew = true
    var dateStart : String =""
    var dateEnd : String = ""
    var dividerClickListener : onDividerClickedListener? = null

    val caption : String
    get() {
        var value = if(isShowNew) "Новые " else ""
        value += when(periodSelection) {
            R.id.rb_day -> "+" + app.getString(R.string.periodDay)
            R.id.rb_month -> "+" + app.getString(R.string.periodMonth)
            R.id.rb_week -> "+" + app.getString(R.string.periodWeek)
            else -> {
                val delim = if(dateStart.isEmpty() || dateEnd.isEmpty()) "" else "-"
                dateStart+delim+dateEnd
            }
        }
        return value
    }

    val period : MutableLiveData<Pair<Date?,Date?>> = MutableLiveData()

    init {
        //default period is one month
        val t1 = Date()
        val t2 = Date(t1.time - 30*24*60*60*1000L)
        period.postValue(Pair(t2,t1))
    }

    fun refreshViews() {
        val df = SimpleDateFormat("dd.MM.yyyy")
        val ds = try {
            df.parse(dateStart)
        } catch(e:ParseException) {null}
        val de = try {
            val d = df.parse(dateEnd)
            Date(d.time+(24*60*60-1)*1000)
        } catch(e:ParseException) {null}
        period.postValue(Pair(ds,de))
    }


    fun invalidate(view: PeriodFilterFragment) {
        periodSelection=view.periodSelection.checkedRadioButtonId
        dateStart = view.dateStart.text.toString()
        dateEnd = view.dateEnd.text.toString()
        refreshViews()
    }
}
