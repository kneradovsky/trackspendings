package com.kovidinvo.trackspendings.viewmodels

import android.app.Application
import android.preference.PreferenceManager
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.kovidinvo.trackspendings.App
import com.kovidinvo.trackspendings.data.SpendingType

class SpeedTypesViewModel(application: Application) : AndroidViewModel(application) {
    private val app = application as App
    val speedTypes : MutableLiveData<Set<String>> = MutableLiveData()
    val speedTypesLimit = 10
    val speedTypesLimitPrefKey = "SpeedSpendingTypes"
    private val appprefs = PreferenceManager.getDefaultSharedPreferences(app)
    init {
        val defValue = SpendingType.values().take(speedTypesLimit).map { it.n }.toMutableSet()
        val sstset = appprefs.getStringSet(speedTypesLimitPrefKey, defValue)
        speedTypes.value = sstset
    }
    fun selectItem(item:String,newval : Boolean) : Boolean {
        if(newval && (speedTypes.value?.size ?: speedTypesLimit) > (speedTypesLimit-1))
            return false
        val set = speedTypes.value!!.toMutableSet()
        if(newval) {
            set.add(item)
        } else set.remove(item)
        appprefs.edit().putStringSet(speedTypesLimitPrefKey,set).apply()
        speedTypes.value = set
        return true
    }
}