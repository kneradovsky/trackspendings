package com.kovidinvo.trackspendings.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.kovidinvo.trackspendings.App
import com.kovidinvo.trackspendings.SpendNotification
import com.kovidinvo.trackspendings.data.PeriodType
import com.kovidinvo.trackspendings.data.SpendingType
import org.joda.time.DateTime

typealias StatsData =  Map<Int,StatsSpendType>
typealias StatsSpendType = MutableMap<String,Double>

class SpendAnalyticsViewModel(inapp:Application) : AndroidViewModel(inapp) {
    val app : App = inapp as App
    lateinit var lcowner : LifecycleOwner

    private val updateFunctions = mapOf(
            PeriodType.DAY to ::updateDailyStats,
            PeriodType.WEEK to ::updateWeeklyStats,
            PeriodType.MONTH to ::updateMonthlyStats,
            PeriodType.CUSTOM to ::updateCustomStats
            )

    var periodSpends : LiveData<List<SpendNotification>>? = null
    val periodStats = MutableLiveData<StatsData>()
    val periodTypeHg = MutableLiveData<StatsSpendType>()

    //val lastmonthspends = app.spendDb.spendingsDao().getSpendingsFromDate(DateTime.now().withDayOfMonth(1).millis)
    val dailyStats   = MutableLiveData<StatsData>()
    val weeklyStats = MutableLiveData<StatsData>()
    val monthlyStats = MutableLiveData<StatsData>()
    val typeHistogram = MutableLiveData<StatsSpendType>()


/*
    init {
       lastmonthspends.observeForever { data ->
           updateData(data)
       }
    }
*/

    fun connectPeriodSpends(pt: PeriodType, start: DateTime, end: DateTime? = null) {
        periodSpends?.removeObservers(lcowner)
        periodSpends = when(pt) {
            PeriodType.DAY -> app.spendDb.spendingsDao().getSpendingsByDatesLive(start.withTimeAtStartOfDay().millis,start.plusDays(1).withTimeAtStartOfDay().millis)
            PeriodType.WEEK -> app.spendDb.spendingsDao().getSpendingsByDatesLive(start.withDayOfWeek(1).withTimeAtStartOfDay().millis,start.plusWeeks(1).withDayOfWeek(1).withTimeAtStartOfDay().millis)
            PeriodType.MONTH -> app.spendDb.spendingsDao().getSpendingsByDatesLive(start.withDayOfMonth(1).withTimeAtStartOfDay().millis,start.plusMonths(1).withDayOfMonth(1).withTimeAtStartOfDay().millis)
            PeriodType.CUSTOM -> app.spendDb.spendingsDao().getSpendingsByDatesLive(start.millis,end?.millis ?: DateTime.now().millis)
        }
        val updateFun = updateFunctions[pt]
        periodSpends?.observe(lcowner, Observer { data ->
            updateFun?.invoke(data,start,end)
        })
    }
    fun updateDailyStats(data:List<SpendNotification>,start:DateTime,end:DateTime?) {
        var ds = mutableMapOf<Int,StatsSpendType>()
        val th = mutableMapOf<String,Double>()
        (0..23).forEach { hour -> ds[hour] = createEmptyStats() }
        for(sp in data) {
            val date = DateTime(sp.date)
            val hour = date.hourOfDay
            ds[hour]!![sp.type] = (ds[hour]!![sp.type] ?: 0.0) + sp.amount
            th[sp.type] = (th[sp.type] ?: 0.0) + sp.amount
        }
        periodTypeHg.postValue(th)
        periodStats.postValue(ds)
    }
    fun updateWeeklyStats(data:List<SpendNotification>,start:DateTime,end:DateTime?) {
        val ws = mutableMapOf<Int,StatsSpendType>()
        val th = mutableMapOf<String,Double>()
        (1..7).forEach { weekDay -> ws[weekDay] = createEmptyStats() }
        for(sp in data) {
            val date = DateTime(sp.date)
            val weekDay = date.dayOfWeek
            ws[weekDay]!![sp.type] = (ws[weekDay]!![sp.type] ?: 0.0) + sp.amount
            th[sp.type] = (th[sp.type] ?: 0.0) + sp.amount
        }
        periodTypeHg.postValue(th)
        periodStats.postValue(ws)
    }
    fun updateMonthlyStats(data:List<SpendNotification>,start:DateTime,end:DateTime?) {
        val ms = mutableMapOf<Int,StatsSpendType>()
        val th = mutableMapOf<String,Double>()
        val lastMonthDay = start.plusMonths(1).withDayOfMonth(1).minusDays(1).dayOfMonth
        (1..lastMonthDay).forEach { mday -> ms[mday] = createEmptyStats() }
        for(sp in data) {
            val date = DateTime(sp.date)
            val mday = date.dayOfMonth
            ms[mday]!![sp.type] = (ms[mday]!![sp.type] ?: 0.0) + sp.amount
            th[sp.type] = (th[sp.type] ?: 0.0) + sp.amount
        }
        periodTypeHg.postValue(th)
        periodStats.postValue(ms)
    }

    fun updateCustomStats(data:List<SpendNotification>,start:DateTime,end:DateTime?) {
    }

    fun updateData(data:List<SpendNotification>) {
        var curdate = DateTime.now().withTimeAtStartOfDay()
        val ds = mutableMapOf<Int,StatsSpendType>()
        val ms = mutableMapOf<Int,StatsSpendType>()
        val ws = mutableMapOf<Int,StatsSpendType>()
        val th = mutableMapOf<String,Double>()
        for(sp in data) {
            val date = DateTime(sp.date)
            if(date.withTimeAtStartOfDay().isEqual(curdate)) {
                val hour = date.hourOfDay
                if(ds[hour]==null) ds[hour]= createEmptyStats()
                ds[hour]!![sp.type]= (ds[hour]!![sp.type] ?: 0.0) + sp.amount
            }
            if(date.withDayOfWeek(1).withTimeAtStartOfDay().isEqual(curdate.withDayOfWeek(1).withTimeAtStartOfDay())) {
                val wday = date.dayOfWeek
                if(ws[wday]==null) ws[wday] = createEmptyStats()
                ws[wday]!![sp.type] = (ws[wday]!![sp.type] ?: 0.0) + sp.amount
            }
            val mday = date.dayOfMonth
            if(ms[mday]==null) ms[mday] = createEmptyStats()
            ms[mday]!![sp.type] = (ms[mday]!![sp.type] ?: 0.0) + sp.amount
            th[sp.type] = (th[sp.type] ?: 0.0) + sp.amount
        }
        dailyStats.postValue(ds)
        weeklyStats.postValue(ws)
        monthlyStats.postValue(ms)
        typeHistogram.postValue(th)
    }

    companion object {
        fun createEmptyStats() : StatsSpendType {
            val v = mutableMapOf<String,Double>()
            SpendingType.values().map {it.n}.onEach { type -> v[type]=0.0 }
            return v
        }

    }

}