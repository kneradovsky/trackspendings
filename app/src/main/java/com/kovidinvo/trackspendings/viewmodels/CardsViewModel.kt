package com.kovidinvo.trackspendings.viewmodels

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.kovidinvo.trackspendings.App



class CardsViewModel(application: Application) : AndroidViewModel(application) {
    val app = application as App
    val cards = app.spendDb.cardsDao().getAll()
    val spendings = app.spendDb.spendingsDao().getSpendingsFromDate(0L)
    val typeHistogram = MutableLiveData<StatsSpendType>()
    val nostatCards : MutableList<Int> = mutableListOf()
    get() = field

    fun updateCardStat(num:Int,nostat:Boolean = true) {
        val update = nostatCards.contains(num) != nostat
        if(nostat) nostatCards.add(num)
        else nostatCards.remove(num)
        if(update)   updateStatData(nostatCards)
    }

    fun updateStatData(nocards: List<Int>) {
        AsyncTask.execute {
            val spends = app.spendDb.spendingsDao().getSpendingsHistogramByNotCards(nocards)
            val histogram: StatsSpendType = spends.map { Pair(it.type, it.amount) }.toMap().toMutableMap()
            typeHistogram.postValue(histogram)
        }
    }




}
