package com.kovidinvo.trackspendings.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.kovidinvo.trackspendings.App
import com.kovidinvo.trackspendings.Budget
import com.kovidinvo.trackspendings.SpendNotification
import java.util.*

class BudgetViewModel(application: Application) : AndroidViewModel(application) {
    val app = application as App
    val activeBudgets = app.spendDb.budgetDao().getAllActiveLive()
    val pastBudgets = app.spendDb.budgetDao().getAllInactiveLive()
    val budgetSpendings : MutableLiveData<List<SpendNotification>> = MutableLiveData()

    fun selectBudget(b: Budget) {
        app.spendDb.queryExecutor.execute {
            val spendings = app.spendDb.budgetDao().getAllSpendingsInBudget(b.start.time,b.end.time,b.type)
            budgetSpendings.postValue(spendings)
        }

    }
}
