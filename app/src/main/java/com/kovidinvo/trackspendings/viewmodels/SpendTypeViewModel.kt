package com.kovidinvo.trackspendings.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.kovidinvo.trackspendings.App
import com.kovidinvo.trackspendings.SpendNotification
import java.util.*


class SpendTypeViewModel(application: Application) : AndroidViewModel(application) {
    val app = application as App
    val SpendHistogram : MutableLiveData<List<SpendNotification>> = MutableLiveData()
    val totalAmount : MutableLiveData<Double> = MutableLiveData()

    init {
        setDataFilter(date=Date().time-30*24*60*60*1000L)
    }

    fun setDataFilterPeriod(card: Int?=null,date1:Long?=null,date2:Long?=null) {
        if(date1==null) return setDataFilter(card,date2,true)
        if(date2==null) return setDataFilter(card,date1,false)
        var query = "select type,sum(amount) from spendings "
        val predicate : MutableList<String> = mutableListOf()
        if(card!=null) predicate.add("card=${card}")
        predicate.add("date between ${date1} and ${date2}")
        val predstr = if(predicate.size>0) "where "+predicate.joinToString(" and ") else ""
        query+=predstr + " group by type"
        processQuery(card,query)
    }

    fun setDataFilter(card:Int?=null,date:Long?=null,before:Boolean=false) {
        var query = "select type,sum(amount) from spendings "
        val predicate : MutableList<String> = mutableListOf()
        if(card!=null) predicate.add("card=${card}")
        if(date!=null) {
            val dir = if(before) "<=" else ">="
            predicate.add("date ${dir} ${date}")
        }
        val predstr = if(predicate.size>0) "where "+predicate.joinToString(" and ") else ""
        query+=predstr + " group by type"
        processQuery(card,query)
    }

    private fun processQuery(card: Int?,query:String) {
        var total = 0.0
        app.spendDb.queryExecutor.execute {
            val cur = app.spendDb.query(query, emptyArray())
            if(cur.count==0) {
                SpendHistogram.postValue(emptyList())
                totalAmount.postValue(0.0)
                return@execute
            }
            cur.moveToFirst()
            val res : MutableList<SpendNotification> = mutableListOf()
            do {
                var sp = SpendNotification(bank="",card=card?:0,pos="",date=Date(),type=cur.getString(0),amount=cur.getDouble(1))
                total+=sp.amount
                res.add(sp)
            } while(cur.moveToNext())
            cur.close()
            SpendHistogram.postValue(res)
            totalAmount.postValue(total)
        }
    }

}
