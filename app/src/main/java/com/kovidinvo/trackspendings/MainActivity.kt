package com.kovidinvo.trackspendings

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.provider.Settings
import android.provider.Telephony
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.Navigation
import androidx.navigation.ui.setupWithNavController
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.firebase.functions.FirebaseFunctions
import com.kovidinvo.trackspendings.databinding.ActivityMainBinding
import com.kovidinvo.trackspendings.fragments.MapsFragment
import com.kovidinvo.trackspendings.services.SmsMsg
import com.kovidinvo.trackspendings.services.SmsReceiver
import com.kovidinvo.trackspendings.viewmodels.PeriodFilterViewModel
import com.kovidinvo.trackspendings.viewmodels.TotalViewModel
import io.fabric.sdk.android.Fabric
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.badge_bottom.view.*
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.withContext
import org.joda.time.DateTime
import java.util.*
import kotlin.coroutines.coroutineContext

class MainActivity : AppCompatActivity() {

    companion object {
        val BROADCAST_RECEIVER_INTENT = "com.kovidinvo.trackspendings.broadcasts.mainactivity"
        fun createBottomBadgeMessage(viewId: Int,text: String="",show:Boolean=true) : Intent {
            return Intent(BROADCAST_RECEIVER_INTENT).apply {
                putExtra("message","bottomBadge")
                putExtra("viewId",viewId)
                putExtra("text",text)
                putExtra("show",show)
            }
        }
        fun createImportSMSMessage(periodId: Int) : Intent {
            return Intent(BROADCAST_RECEIVER_INTENT).apply {
                putExtra("message","bottomBadge")
                putExtra("periodId",periodId)
            }
        }
    }

    private val MY_PERMISSION_REQUEST_SMS = 65533
    private lateinit var appref : App
    private val firefunc: FirebaseFunctions by lazy { FirebaseFunctions.getInstance()}
    private val TAG = "MainActivity"
    private var isRationaleShown = false

    val app : App
    get() {
        return appref
    }

    private var lastSmsTime: Long = 0

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if(intent?.action == BROADCAST_RECEIVER_INTENT && intent?.hasExtra("message"))
                processMessage(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        lastSmsTime = savedInstanceState?.getLong("lastSmsTimestamp",0) ?: 0
        appref = application as App
        super.onCreate(savedInstanceState)

        //set guid
        appref.authGuid=Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

        //crashlytics
        val crashlyticsKit = Crashlytics.Builder()
            .core(CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
            .build()
        Fabric.with(this, crashlyticsKit)

        val binding : ActivityMainBinding = DataBindingUtil.setContentView(this,R.layout.activity_main)

        val navController = Navigation.findNavController(this,R.id.main_nav_fragment)


        binding.bottomNavigation.setupWithNavController(navController)


        binding.navigationView.setupWithNavController(navController)

        phPermissionsRationale.visibility = View.GONE

        checkAndRequestPermissions()

        appref.subjSMS.subscribe { sms -> lastSmsTime = sms.date.time} != null

        btnPermsOK.setOnClickListener { isRationaleShown=true;phPermissionsRationale.visibility=View.GONE;checkAndRequestPermissions() }
        //register receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, IntentFilter(BROADCAST_RECEIVER_INTENT))
    }


    private fun processMessage(intent: Intent) {
        Log.i(TAG,"external event received ${intent.getStringExtra("message")}")
        when(intent.getStringExtra("message")) {
            "bottomBadge" -> {
                val viewId = intent.getIntExtra("viewId",-1)
                val text = intent.getStringExtra("text") ?: ""
                if(viewId<0) return
                if(text.isEmpty()) removeBottomBadge(viewId)
                else showBottomBadge(viewId,text)
            }
            "importSMS" -> {
                val periodId = intent.getIntExtra("periodId",-1)
                if(periodId<0) return
                readOldSms(periodId)
            }
        }
    }


    private fun checkAndRequestPermissions() : Boolean {
        val permlist = listOf(Manifest.permission.READ_SMS,Manifest.permission.RECEIVE_SMS,
                Manifest.permission.ACCESS_FINE_LOCATION)
        val needRequestPerm = permlist.filter { perm -> ContextCompat.checkSelfPermission(this,perm) != PackageManager.PERMISSION_GRANTED }
                .count()
        if(needRequestPerm>0) {
            ActivityCompat.requestPermissions(this, permlist.toTypedArray(), MY_PERMISSION_REQUEST_SMS)
            return false
        }
        return true
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode) {
            MY_PERMISSION_REQUEST_SMS -> {
                if(grantResults.count { res -> res != PackageManager.PERMISSION_GRANTED} == 0)
                {
                    Log.d(TAG,"Permissions granted")
                    readOldSmsIfNeeded() //read old sms
                } else {
                    Log.d(TAG,"Permissions denied")
                    isRationaleShown=false
                }
            }
            else -> {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putLong("lastSmsTimestamp",lastSmsTime)
        super.onSaveInstanceState(outState)
    }


    private fun readOldSmsIfNeeded() {
        val prefs = getPreferences(Context.MODE_PRIVATE)
        val shoudAsk = prefs.getBoolean("shouldAskToReadOldSms",true)
        if(shoudAsk) {
            val builder = AlertDialog.Builder(this)
            val itemsArray = arrayOf(R.string.periodMonth,R.string.periodWeek1,R.string.periodDay1,R.string.doNotLoad)
            builder.setTitle(R.string.dlgReadOldSmsTitle)
                    .setItems(itemsArray.map(::getString).toTypedArray(),{dialog, id -> readOldSms(itemsArray[id])})
            builder.create().show()
            prefs.edit().putBoolean("shouldAskToReadOldSms",false).apply()
        }
    }

    private fun readOldSms(answerId:Int) {
        val startPeriod = when(answerId) {
            R.string.periodMonth -> DateTime.now().minusDays(30).withTimeAtStartOfDay().millis
            R.string.periodWeek1 -> DateTime.now().minusDays(7).withTimeAtStartOfDay().millis
            R.string.periodDay1 -> DateTime.now().minusDays(1).withTimeAtStartOfDay().millis
            else -> return
        }
        AsyncTask.execute {
            val smscur = contentResolver.query(Uri.parse("content://sms/inbox"),arrayOf("address","date","body"),
                "date > ?",arrayOf(startPeriod.toString()),null) ?: return@execute
            runOnUiThread {  phProgressBar.visibility=View.VISIBLE }
            val curCount = smscur.count
            var curindex = 0
            SmsReceiver.doNotUpdateCards=true
            val buffer = mutableListOf<SpendNotification>()
            while(smscur.moveToNext()) {
                curindex++
                runOnUiThread { mainProgressBar.progress = curindex/curCount*100 }
                val address = smscur.getString(0)
                val date = smscur.getLong(1)
                val body = smscur.getString(2)
                val sm = SmsMsg(address,date,body)
                if(SmsReceiver.processors.containsKey(address)) {
                    val sp = SmsReceiver.processors[address]?.invoke(sm) ?: continue
                    buffer.add(sp)
                    if(buffer.size>=10) {
                        appref.spendDb.spendingsDao().insertAll(buffer)
                        buffer.clear()
                    }
                }
            }
            appref.spendDb.spendingsDao().insertAll(buffer)
            SmsReceiver.doNotUpdateCards=false
            runOnUiThread {
                phProgressBar.visibility=View.GONE
                val primaryFrag = this.main_nav_fragment?.childFragmentManager?.primaryNavigationFragment
                if(primaryFrag is MapsFragment) {
                    if(primaryFrag!=null) {
                        val vmPeriodFilter = ViewModelProviders.of(primaryFrag).get(PeriodFilterViewModel::class.java)
                        vmPeriodFilter.refreshViews()
                    }
                }
            }
        }

    }

    private fun showBottomBadge(viewId : Int, text: String) {
        removeBottomBadge(viewId)
        val itemView = bottom_navigation.findViewById<BottomNavigationItemView>(viewId)
        val badge = LayoutInflater.from(this).inflate(R.layout.badge_bottom,bottom_navigation,false)
        badge.badge_text_view.text = text
        itemView.addView(badge)
    }

    private fun removeBottomBadge(viewId : Int) {
        val itemView = bottom_navigation.findViewById<BottomNavigationItemView>(viewId)
        if(itemView.childCount == 3) itemView.removeViewAt(2)
    }
}
