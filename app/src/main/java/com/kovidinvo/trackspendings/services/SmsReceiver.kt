    package com.kovidinvo.trackspendings.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.telephony.SmsMessage
import android.util.Log
import com.kovidinvo.trackspendings.PayCard
import com.kovidinvo.trackspendings.SpendNotification
import com.kovidinvo.trackspendings.SpendingsDatabase
import com.kovidinvo.trackspendings.data.Bank
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.Subject
import java.util.*


data class SmsMsg(val originatingAddress: String, val timestampMillis:Long, val messageBody:String)


class SmsReceiver : BroadcastReceiver() {


    override fun onReceive(ct: Context?, intent: Intent) {
        val data = intent.extras
        val pdus = data?.get("pdus") as Array<Any>
        pdus.forEach { pdu : Any ->
            val format = data.getString("format")
            val sms = if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M) {
                SmsMessage.createFromPdu(pdu as ByteArray,format)
            } else {
                SmsMessage.createFromPdu(pdu as ByteArray)
            }
            val smscont = SmsMsg(sms.originatingAddress, sms.timestampMillis, sms.messageBody)
            val sender = sms.originatingAddress
            if(processors.containsKey(sender)) {
                try {
                    val sn = processors[sender]!!.invoke(smscont)
                    if (sn != null)
                        subjSms?.onNext(sn)
                } catch(e:Exception) {
                    Log.w(TAG,"SMS processing failed",e)
                }
            }
        }
    }

    companion object {
        private val TAG = "SmsReceiver"
        private var subjSms : Subject<SpendNotification>? = null
        lateinit var localDb : SpendingsDatabase
        var doNotUpdateCards = false
        fun attachSubject(insubj: Subject<SpendNotification>) {
            subjSms = insubj
        }

        val processors : Map<String,(SmsMsg) -> SpendNotification?>
        val processorsData : MutableMap<String,String> = mutableMapOf()
        val bankNames = hashMapOf(
                "900" to Bank.SBERBANK.n,
                "OTKRITIE" to Bank.OTKRITIE.n,
                "Tinkoff" to Bank.TINKOFF.n,
                "Alfa-Bank" to Bank.ALFABANK.n,
                "VTB" to Bank.VTB.n
        )

        init {
            processors = hashMapOf(
                    "900" to Companion::processorSberbank,
                    "OTKRITIE" to Companion::processorOpenbank,
                    "Tinkoff" to Companion::processorTinkoff,
                    "Alfa-Bank" to Companion::processorAlfabank,
                    "VTB" to Companion::processorVTB

            )
        }

        @SuppressWarnings("CheckResult")
        private fun updateCard(pc: PayCard) {
            if(doNotUpdateCards) return //return if we don't need to update cards
            val onSuccess = Consumer<PayCard> { dbpc -> localDb.cardsDao().update(pc.copy(pcid=dbpc.pcid)) }
            val onNotFound = Action { localDb.cardsDao().insert(pc)}
            val onError = Consumer<Throwable> { e -> Log.d(TAG,"select card by num error",e)}
            localDb.cardsDao().cardByNum(pc.bank,pc.num)
                    .observeOn(Schedulers.io())
                    .subscribeOn(Schedulers.io())
                    .subscribe(onSuccess,onError,onNotFound)
        }

        private fun processorSberbank(sms: SmsMsg) : SpendNotification? {
            val bankName= bankNames[sms.originatingAddress]!!
            val messageBody = if(processorsData.containsKey(Bank.SBERBANK.n)) processorsData[Bank.SBERBANK.n] + sms.messageBody else sms.messageBody
            val openrx = Regex("^\\D+(\\d{4})\\s+([\\d\\:]+)\\s+[Пп]окупка\\s+([\\d\\.]+)[рp]+\\s+(.*?)\\s+[Бб]аланс:\\s+([\\d\\.]+)[рp]+.*")
            var res = openrx.matchEntire(messageBody)
            if(res!=null) {
                val opdate = Date(sms.timestampMillis)
                val amount = res.groupValues[3].toDouble()
                val pos = res.groupValues[4]
                val cardnum = res.groupValues[1].toInt()
                updateCard(PayCard(num = cardnum, bank = bankName, balance = res.groupValues[5].toDouble()))
                processorsData.remove(Bank.SBERBANK.n)
                return SpendNotification(bank = bankName, pos = pos, date = opdate, amount = amount, card = cardnum)
            }
            if(sms.messageBody.contains("окупка")) {
                //maybe it's splitted sms, wait for the second part
                processorsData[Bank.SBERBANK.n]=sms.messageBody
            }
            return null
        }

        private fun processorTinkoff(sms: SmsMsg) : SpendNotification? {
            val bankName= bankNames[sms.originatingAddress]!!
            val parts = sms.messageBody.split(". ")

            if(!sms.messageBody.startsWith("Покупка")) return null
            if(parts.size<5) {
                return null
            }
            val cardNum = if(parts[1].toLowerCase().contains("бесконтактная оплата")) {0}
                        else {parts[1].substringAfter('*').toInt()}
            val amount = parts[2].substringBefore(' ').toDouble()
            val pos = parts[3].replace("\"","")
            val balance = parts[4].split(" ")[1].toDouble()
            val opdate = Date(sms.timestampMillis)
            updateCard(PayCard(bank = bankName, balance = balance, num = cardNum))
            return SpendNotification(bank = "Tinkoff", pos = pos, date = opdate, amount = amount, card = cardNum)
        }

        private fun processorAlfabank(sms: SmsMsg) : SpendNotification? {
            val bankName= bankNames[sms.originatingAddress]!!
            val parts = sms.messageBody.split("; ")
            if(parts.size<7)
                return null
            if(parts[1]!="Pokupka" || parts[2] != "Uspeshno")
                return null
            val cardNum = parts[0].substringAfterLast("*").toInt()
            val opdate = Date(sms.timestampMillis)
            val amount = parts[3].split(" ")[1].replace(",",".").toDouble()
            val balance = parts[4].split(" ")[1].replace(",",".").toDouble()
            val pos = parts[5]
            updateCard(PayCard(bank = bankName, balance = balance, num = cardNum))
            return SpendNotification(bank = bankName, pos = pos, date = opdate, amount = amount, card = cardNum)
        }
        private fun processorOpenbank(sms: SmsMsg) : SpendNotification? {
            val bankName= bankNames[sms.originatingAddress]!!
            val openrx = Regex("^Платёж\\s+([\\d \\.]+)\\s+р\\.\\s+в\\s+(.*)\\.\\s+Карта\\s+\\*(\\d{4})\\.\\s+Доступно\\s+([\\d \\.]+).*")
            val res = openrx.matchEntire(sms.messageBody)
            if(res!=null) {
                val opdate = Date(sms.timestampMillis)
                val amount = res.groupValues[1].replace(" ","").toDouble()
                val pos = res.groupValues[2]
                val cardNum = res.groupValues[3].toInt()
                val balance = res.groupValues[4].replace(" ","").toDouble()
                updateCard(PayCard(bank = bankName, balance = balance, num = cardNum))
                return SpendNotification(bank = bankName, pos = pos, date = opdate, amount = amount, card = cardNum)
            }
            return null
        }
        private fun processorVTB(sms: SmsMsg) : SpendNotification? {
            val bankName= bankNames[sms.originatingAddress]!!
            val rx = Regex("^Karta\\s+\\*(\\d{4}):\\s+Oplata\\s+([\\d\\.]+)\\s+RUB;([^;]+);.*?dostupno\\s+([\\d\\.]+)\\s+RUB")
            val res = rx.matchEntire(sms.messageBody)
            if(res!=null) {
                val opdate = Date(sms.timestampMillis)
                val cardNum = res.groupValues[1].toInt()
                val amount = res.groupValues[2].toDouble()
                val pos = res.groupValues[3]
                val balance = res.groupValues[4].toDouble()
                updateCard(PayCard(bank = bankName, balance = balance, num = cardNum))
                return SpendNotification(bank = bankName, pos = pos, date = opdate, amount = amount, card = cardNum)
            }
            return null
        }
    }
}


