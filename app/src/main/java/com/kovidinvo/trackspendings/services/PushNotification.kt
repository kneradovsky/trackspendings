package com.kovidinvo.trackspendings.services

import android.accessibilityservice.AccessibilityService
import android.app.Notification
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import android.util.Log
import android.view.accessibility.AccessibilityEvent
import com.kovidinvo.trackspendings.App


class PushNotification : NotificationListenerService() {

    override fun onNotificationPosted(sbn: StatusBarNotification?) {
        val intent = Intent(PushNotificationIntentReceiver.intentName)
        intent.putExtra("notification",sbn)
        sendBroadcast(intent)
        super.onNotificationPosted(sbn)
    }

    override fun onNotificationRemoved(sbn: StatusBarNotification?) {
        super.onNotificationRemoved(sbn)
    }

    override fun onNotificationPosted(sbn: StatusBarNotification?, rankingMap: RankingMap?) {
        Log.i("PushListener","Package=${sbn?.packageName}, Text=${sbn?.notification?.tickerText}")
        super.onNotificationPosted(sbn, rankingMap)
    }


}


class AcessibilityNotifications : AccessibilityService() {
    override fun onInterrupt() {
        return
    }

    override fun onAccessibilityEvent(event: AccessibilityEvent?) {
        val notification = event?.parcelableData as Notification
        val intent = Intent(PushNotificationIntentReceiver.intentName)
        intent.putExtra("package",event.packageName)
        intent.putExtra("text",notification.tickerText)
        intent.putExtra("notifextra",notification.extras)
        sendBroadcast(intent)
        return
    }
}

class PushNotificationIntentReceiver(val app: App) : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if(intent==null || intent.extras==null) return
        val pkg = intent.extras.getString("package")
        val text = intent.extras.getString("text")
        val extra = intent.extras.getBundle("notifextra")
        Log.i("PushReceiver","Package=$pkg Text=$text, Extras:")
        extra.keySet()?.forEach { key ->
            Log.i("PushReceiver", key + "=" + extra.get(key)?.toString())
        }
    }
    companion object {
        val intentName = "com.kovidinvo.trackspendings.intents.NOTIFICATION_POSTED_INTENT"
    }
}