package com.kovidinvo.trackspendings.services

import android.content.BroadcastReceiver
import android.content.Intent
import android.content.Context
import android.util.Log
import androidx.work.*
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.SetOptions

import com.kovidinvo.trackspendings.App
import com.kovidinvo.trackspendings.Budget
import com.kovidinvo.trackspendings.data.SpendTypeData
import com.kovidinvo.trackspendings.data.SpendingType
import org.joda.time.DateTime
import java.util.*
import java.util.concurrent.TimeUnit

class OnBootBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(ctx: Context?, intent: Intent) {
        startOnBootJob()
    }

    companion object {

        fun startOnBootJob() {
            val req = PeriodicWorkRequestBuilder<OnBootServiceWorker>(1,TimeUnit.HOURS)
                    .build()
//            val req = OneTimeWorkRequestBuilder<OnBootServiceWorker>()
//                    .build()

            WorkManager.getInstance()
                    .enqueueUniquePeriodicWork(
                            "com.kovidinvo.trackspendings.OnBootServiceOneTime",
                            ExistingPeriodicWorkPolicy.REPLACE,
                            req
                    )

//                    .enqueueUniqueWork(
//                    "com.kovidinvo.trackspendings.OnBootServiceOneTime",
//                    ExistingWorkPolicy.REPLACE,
//                    req
//                    )
        }
    }

}

class OnBootServiceWorker(val ctx : Context,val params : WorkerParameters) : Worker(ctx,params) {
    private val mDb : FirebaseFirestore by lazy { FirebaseFirestore.getInstance()}
    override fun doWork(): Result {
        refreshBudgets()
        return Result.SUCCESS
    }
    private fun refreshBudgets() {
        val app = ctx as App
        val curdate = Calendar.getInstance()
        if(curdate.get(Calendar.HOUR_OF_DAY)!=0) return
        val bglist = app.spendDb.budgetDao().getAllActive().blockingFirst()
        if(bglist.isEmpty()) return
        val curTime = curdate.timeInMillis
        val bgs2update = bglist.filter { bg -> bg.end.time <= curTime }.onEach { bg -> bg.active=0 }.toList()
        if(bgs2update.isEmpty()) return

        app.spendDb.budgetDao().updateMany(bgs2update)

    }
}

