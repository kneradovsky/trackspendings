package com.kovidinvo.trackspendings

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.NotificationCompat
import androidx.core.app.TaskStackBuilder
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.multidex.MultiDexApplication
import androidx.navigation.NavDeepLinkBuilder
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.functions.FirebaseFunctions
import com.kovidinvo.trackspendings.data.DrawableImageProvider
import com.kovidinvo.trackspendings.data.SpendTypeData
import com.kovidinvo.trackspendings.services.OnBootBroadcastReceiver
import com.kovidinvo.trackspendings.services.PushNotificationIntentReceiver
import com.kovidinvo.trackspendings.services.SmsReceiver
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import org.joda.time.DateTime


class App : MultiDexApplication() {
    private val BUDGET_NOTIFICATION_ID=1024
    val NOTIFICATION_CHANNEL_ID="com.kovidinvo.trackspendings.mainchannel"

    init {
    }


    val onNewSpending = Consumer<SpendNotification> { notif ->
        val dao = spendDb.spendingsDao()
        dao.insert(notif)
        if (notif.longitude != null) return@Consumer //reentrant notification
        try {
            locationClient.lastLocation.addOnSuccessListener { loc ->
                if (loc == null) {
                    Log.w(TAG, "Location get failed. Location is null")
                    notif.lattitude = 100002.0
                    notif.longitude = 100002.0
                    updateSpendNotification(notif)
                    return@addOnSuccessListener
                }
                notif.lattitude = loc.latitude
                notif.longitude = loc.longitude
                updateSpendNotification(notif)
            }.addOnFailureListener { ex ->
                Log.d(TAG, "location failed", ex)
                notif.lattitude = 100001.0
                notif.longitude = 100001.0
                updateSpendNotification(notif)
            }
        } catch (e: SecurityException) {
            Log.d(TAG, "Location exception", e)
        }
    }

    private fun updateSpendNotification(notif:SpendNotification) {
        val dao = spendDb.spendingsDao()
        AsyncTask.execute { dao.update(notif) }
        subjSMS.onNext(notif)
        AsyncTask.execute {
            val pos = spendDb.posDao().getPOSByName(notif.pos).blockingGet()
            if (pos != null) {
                val prevType=notif.type
                notif.type = pos.type
                if(prevType!=notif.type) subjSMS.onNext(notif)
            }
            updateBudgets(notif)
            updateSpendingFirebase(notif)
        }
    }

    fun updateBudgets(sp:SpendNotification,prevType:String="") {
        if(!prevType.isEmpty()) {
            //decrease budget by notif value
            val upcnt = spendDb.budgetDao().updateActiveBudgets((-1)*sp.amount,"%${prevType}%")
            Log.d(TAG,"Decrease budgets: updated ${upcnt} records")
        }
        spendDb.budgetDao().updateActiveBudgets(sp.amount,"%${sp.type}%")
        val almostFullBudgets = spendDb.budgetDao().getActiveBudgetsByType("%${sp.type}%")
                .filter { b -> (b.actual/b.planned)>0.9}


        val notifText = almostFullBudgets
                .map { b ->
                    var budgetLeft = b.planned-b.actual
                    if(budgetLeft<0) budgetLeft=0.0
            String.format(getString(R.string.budgetRemaining,b.name,budgetLeft,DateTime(b.end).toString("dd.MM.yyyy")))
        }.joinToString("\n")
        if(notifText.isNotEmpty()) {
            LocalBroadcastManager.getInstance(this)
                    .sendBroadcast(MainActivity.createBottomBadgeMessage(R.id.budgetFragment,almostFullBudgets.size.toString()))
            val builder = NotificationCompat.Builder(this,NOTIFICATION_CHANNEL_ID)

            val pendingIntent = NavDeepLinkBuilder(this)
                    .setGraph(R.navigation.nav_spendings)
                    .setDestination(R.id.budgetFragment)
                    .createPendingIntent()
            builder.setSmallIcon(R.drawable.ic_menu_analytics)
                    .setContentTitle(getString(R.string.budgetAlmostEmpty))
                    .setContentText(notifText)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
            val notifManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notifManager?.notify(BUDGET_NOTIFICATION_ID,builder.build())
        }

    }

    override fun onCreate() {
        super.onCreate()
        fireFuncs = FirebaseFunctions.getInstance()
        spendDb = SpendingsDatabase.getInstance(applicationContext)
        //smsreceiver
        locationClient = LocationServices.getFusedLocationProviderClient(this)
        SmsReceiver.attachSubject(subjSMS)
        SmsReceiver.localDb = spendDb
        val a = subjSMS.subscribeOn(Schedulers.io()).observeOn(Schedulers.io())
                .subscribe(onNewSpending)


        //create notification channel
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create the NotificationChannel
            val name = getString(R.string.notification_channel_name)
            val descriptionText = getString(R.string.notification_channel_description)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val mChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID, name, importance)
            mChannel.description = descriptionText
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(mChannel)
        }
        //start on boot service
        OnBootBroadcastReceiver.startOnBootJob()
        //start notification listener receiver
        //val notifreceiver = PushNotificationIntentReceiver(this)
        //val intentFilter = IntentFilter()
        //intentFilter.addAction(PushNotificationIntentReceiver.intentName)
        //registerReceiver(notifreceiver,intentFilter)
    }



    fun updateSpendingFirebase(sp: SpendNotification) {
        val param = sp.toJSONObject()
        if(authGuid!=null)
            param.put("aguid",authGuid)
        param.put("card",0) //delete pay card last digits
        fireFuncs.getHttpsCallable("storeSpending")
                .call(param)
                .addOnCompleteListener { task ->
                    if(task.isSuccessful) Log.d(TAG,"storeSpending finished successfuly")
                    else {
                        Log.d(TAG,"storeSpending failed",task.exception)
                    }
                }
    }



    val subjSMS: Subject<SpendNotification> = PublishSubject.create()
    val messages : Subject<Bundle> = PublishSubject.create()

    lateinit var fireFuncs : FirebaseFunctions

    private val TAG = "trackspendings.App"

    lateinit var spendDb: SpendingsDatabase
        private set

    var authGuid : String? = null

    private lateinit var locationClient: FusedLocationProviderClient

    val imageProviders: Map<String, DrawableImageProvider> by lazy {
        SpendTypeData.resMapIds.entries.map { e -> Pair(e.key, DrawableImageProvider(this, e.value)) }.toMap()
    }

}