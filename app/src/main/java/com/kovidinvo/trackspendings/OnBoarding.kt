package com.kovidinvo.trackspendings

import android.app.ActionBar
import android.os.Bundle
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.view.children
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.Navigation
import com.google.android.material.button.MaterialButton
import com.kovidinvo.trackspendings.data.BankData
import com.kovidinvo.trackspendings.data.DrawableImageProvider

import kotlinx.android.synthetic.main.activity_on_boarding.*

class OnBoarding : FragmentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val startedByUser = intent.extras?.getBoolean("startedByUser") ?: false

        val skipOnBoarding = getSharedPreferences("OnBoardingActivity",Context.MODE_PRIVATE)
                .getBoolean(OnBoardingFragment.skipOnBoardingPref,false)
        if(!startedByUser && skipOnBoarding) OnBoardingFragment.exitOnBoarding(this)

        setTheme(R.style.AppTheme)
        setContentView(R.layout.activity_on_boarding)
        onBoardingPager.adapter=OnBoardingPagerAdapter(supportFragmentManager)
        tabOnBoarding.setupWithViewPager(onBoardingPager,true)
    }



}

class OnBoardingPagerAdapter(fm:FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getCount() : Int = OnBoardingFragment.LayoutsResId.size
    override fun getItem(pos:Int) : Fragment = OnBoardingFragment.newInstance(pos)
}

class OnBoardingFragment() : Fragment() {
    var layout : Int = R.layout.onboarding_page1

    override fun onCreate(savedInstanceState: Bundle?) {
        layout = arguments?.getInt("page") ?:  R.layout.onboarding_page1
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layout,container,false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val btn = view?.findViewById<MaterialButton>(R.id.btnOnBoardingClose)
        if(btn!=null) {
            btn.setOnClickListener { exitOnBoarding(context!!) }
        }
        val phBankLogos = view?.findViewById<LinearLayout>(R.id.phBankLogos)
        if(phBankLogos!=null) {
            BankData.logosList.map {
                ImageView(context).apply {
                    setImageResource(it.value);
                    setPadding(DrawableImageProvider.dp2px(4.0f).toInt(),0,0,0)
                }
            }.forEach { phBankLogos.addView(it) }
        }

    }


    companion object {
        val skipOnBoardingPref = "skipOnBoarding"
        fun newInstance(pos:Int) : Fragment {
            val args = Bundle()
            args.putInt("page", LayoutsResId[pos])
            return OnBoardingFragment().apply { arguments = args }
        }
        val LayoutsResId = listOf(
                R.layout.onboarding_page1,
                R.layout.onboarding_page2,
                R.layout.onboarding_page3,
                R.layout.onboarding_page4,
                R.layout.onboarding_page5,
                R.layout.onboarding_page6
        )
        fun exitOnBoarding(ctx: Context) {
            ctx.getSharedPreferences("OnBoardingActivity",Context.MODE_PRIVATE).edit().putBoolean(skipOnBoardingPref,true).apply()
            val intent = Intent(ctx,MainActivity::class.java).
                    apply { addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)}
            ctx.startActivity(intent)
            (ctx as? Activity)?.finish()
        }

    }
}

