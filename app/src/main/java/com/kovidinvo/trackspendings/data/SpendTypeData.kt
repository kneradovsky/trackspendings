package com.kovidinvo.trackspendings.data

import android.app.Application
import android.content.res.Resources
import android.graphics.Color
import androidx.core.content.res.ResourcesCompat
import com.kovidinvo.trackspendings.R

enum class SpendingType(val n:String) {
    CAFE("cafe"),
    FOOD("food"),
    SHOPPING("shopping"),
    BEAUTY("beauty"),
    KIDS("kids"),
    PETSHOP("petshop"),
    AUTOMOTIVE("automotive"),
    TRANSPORT("transport"),
    EDUCATION("education"),
    BUSINESS("business"),
    UNKNOWN("unknown"),
    CONNECTION("connection"),
    HEALTHCARE("healthcare"),
    PHARMACY("pharmacy"),
    HOUSE("house")
}

class SpendTypeData {
    companion object {
        val resIds = mapOf(
                SpendingType.AUTOMOTIVE.n to R.drawable.ic_type_automotive,
                SpendingType.CAFE.n to R.drawable.ic_type_cafe,
                SpendingType.CONNECTION.n to R.drawable.ic_type_connection,
                SpendingType.FOOD.n to R.drawable.ic_type_food,
                SpendingType.SHOPPING.n to R.drawable.ic_type_shopping,
                SpendingType.TRANSPORT.n to R.drawable.ic_type_transport,
                SpendingType.UNKNOWN.n to R.drawable.ic_type_unknown,
                SpendingType.BUSINESS.n to R.drawable.ic_type_business,
                SpendingType.PHARMACY.n to R.drawable.ic_type_pharmacy,
                SpendingType.HOUSE.n to R.drawable.ic_type_house,
                SpendingType.BEAUTY.n to R.drawable.ic_type_beauty,
                SpendingType.EDUCATION.n to R.drawable.ic_type_education,
                SpendingType.PETSHOP.n to R.drawable.ic_type_petshop,
                SpendingType.HEALTHCARE.n to R.drawable.ic_type_healthcare,
                SpendingType.KIDS.n to R.drawable.ic_type_kids
        )
        val resMapIds = mapOf(
                SpendingType.AUTOMOTIVE.n to R.drawable.ic_maptype_automotive,
                SpendingType.CAFE.n to R.drawable.ic_maptype_cafe,
                SpendingType.CONNECTION.n to R.drawable.ic_maptype_connection,
                SpendingType.FOOD.n to R.drawable.ic_maptype_food,
                SpendingType.SHOPPING.n to R.drawable.ic_maptype_shopping,
                SpendingType.TRANSPORT.n to R.drawable.ic_maptype_transport,
                SpendingType.UNKNOWN.n to R.drawable.ic_maptype_unknown,
                SpendingType.BUSINESS.n to R.drawable.ic_maptype_business,
                SpendingType.PHARMACY.n to R.drawable.ic_maptype_pharmacy,
                SpendingType.HOUSE.n to R.drawable.ic_maptype_house,
                SpendingType.BEAUTY.n to R.drawable.ic_maptype_beauty,
                SpendingType.EDUCATION.n to R.drawable.ic_maptype_education,
                SpendingType.PETSHOP.n to R.drawable.ic_maptype_petshop,
                SpendingType.HEALTHCARE.n to R.drawable.ic_maptype_healthcare,
                SpendingType.KIDS.n to R.drawable.ic_maptype_kids
        )
        val resStrIds = mapOf(
                SpendingType.AUTOMOTIVE.n to R.string.nametype_automotive,
                SpendingType.CAFE.n to R.string.nametype_cafe,
                SpendingType.CONNECTION.n to R.string.nametype_connection,
                SpendingType.FOOD.n to R.string.nametype_food,
                SpendingType.SHOPPING.n to R.string.nametype_shopping,
                SpendingType.TRANSPORT.n to R.string.nametype_transport,
                SpendingType.UNKNOWN.n to R.string.nametype_unknown,
                SpendingType.BUSINESS.n to R.string.nametype_business,
                SpendingType.PHARMACY.n to R.string.nametype_pharmacy,
                SpendingType.HOUSE.n to R.string.nametype_house,
                SpendingType.BEAUTY.n to R.string.nametype_beauty,
                SpendingType.EDUCATION.n to R.string.nametype_education,
                SpendingType.PETSHOP.n to R.string.nametype_petshop,
                SpendingType.HEALTHCARE.n to R.string.nametype_healthcare,
                SpendingType.KIDS.n to R.string.nametype_kids

        )
        val typeColors = mapOf(
                SpendingType.AUTOMOTIVE.n to Color.parseColor("#56317D"),
                SpendingType.CAFE.n to Color.parseColor("#831F28"),
                SpendingType.CONNECTION.n to Color.parseColor("#6FCF97"),
                SpendingType.FOOD.n to Color.parseColor("#E46B2A"),
                SpendingType.SHOPPING.n to Color.parseColor("#F4B927"),
                SpendingType.TRANSPORT.n to Color.parseColor("#1D518F"),
                SpendingType.UNKNOWN.n to Color.parseColor("#3EB7D6"),
                SpendingType.BUSINESS.n to Color.parseColor("#C1840B"),
                SpendingType.PHARMACY.n to Color.parseColor("#2278A9"),
                SpendingType.HOUSE.n to Color.parseColor("#BE9C91"),
                SpendingType.BEAUTY.n to Color.parseColor("#EB5757"),
                SpendingType.EDUCATION.n to Color.parseColor("#251442"),
                SpendingType.PETSHOP.n to Color.parseColor("#8D6E63"),
                SpendingType.HEALTHCARE.n to Color.parseColor("#16693E"),
                SpendingType.KIDS.n to Color.parseColor("#8697CB")

        )
        val bgLargeIds = mapOf(
                SpendingType.AUTOMOTIVE.n to R.drawable.background_automotive_large,
                SpendingType.CAFE.n to R.drawable.background_cafe_large,
                SpendingType.CONNECTION.n to R.drawable.background_connection_large,
                SpendingType.FOOD.n to R.drawable.background_food_large,
                SpendingType.SHOPPING.n to R.drawable.background_shopping_large,
                SpendingType.TRANSPORT.n to R.drawable.background_transport_large,
                SpendingType.UNKNOWN.n to R.drawable.background_unknown_large,
                SpendingType.BUSINESS.n to R.drawable.background_business_large,
                SpendingType.PHARMACY.n to R.drawable.background_pharmacy_large,
                SpendingType.HOUSE.n to R.drawable.background_house_large,
                SpendingType.BEAUTY.n to R.drawable.background_beauty_large,
                SpendingType.EDUCATION.n to R.drawable.background_education_large,
                SpendingType.PETSHOP.n to R.drawable.background_petshop_large,
                SpendingType.HEALTHCARE.n to R.drawable.background_healthcare_large,
                SpendingType.KIDS.n to R.drawable.background_kids_large

        )
    }
}

