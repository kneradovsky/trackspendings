package com.kovidinvo.trackspendings.data

import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Build
import android.util.DisplayMetrics
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.kovidinvo.trackspendings.R
import com.yandex.runtime.image.ImageProvider

fun Context.getBitmapFromDrawable(id : Int) : Bitmap? {
    var drawable = ContextCompat.getDrawable(this, id) ?: return null

    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
        drawable = DrawableCompat.wrap(drawable).mutate()
    }
    //val px10dp = DrawableImageProvider.dp2px(12f).toInt()
    val bitmap = Bitmap.createBitmap(
            (drawable.intrinsicWidth*0.75).toInt(),
            (drawable.intrinsicHeight*0.75).toInt(),
            Bitmap.Config.ARGB_8888) ?: return null
    val canvas = Canvas(bitmap)
    drawable.setBounds(0, 0, canvas.width, canvas.height)
    drawable.draw(canvas)

    return bitmap
}


class DrawableImageProvider(val ctx:Context,val id:Int) : ImageProvider() {
    val img: Bitmap?
    val fallBackImg : Bitmap by lazy {
        ImageProvider.fromResource(ctx, R.drawable.abc_ic_star_black_36dp).image
    }
    init {
        img = ctx.getBitmapFromDrawable(id)
    }
    override fun getId(): String {
        return "drawable_${id}"
    }

    override fun getImage(): Bitmap {
        return img ?: fallBackImg
    }

    companion object {
        val displayMetrics : DisplayMetrics by lazy {
            Resources.getSystem().getDisplayMetrics()
        }
        fun dp2px(dp:Float) :Float {
            return displayMetrics.densityDpi/ 160.0f*dp
        }
    }
}

class ViewImageProvider(val mid: String,view: View) : ImageProvider(false) {
    val img : Bitmap = getBitmapFromView(view)
    override fun getId(): String {
        return mid
    }

    override fun getImage(): Bitmap {
        return img
    }

    companion object {
        fun getBitmapFromView(view: View) : Bitmap {
            view.measure(0, 0);
            val image = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            val canvas = Canvas(image);
            view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
            view.draw(canvas);
            return image
        }
    }
}