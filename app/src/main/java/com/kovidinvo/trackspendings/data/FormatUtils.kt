package com.kovidinvo.trackspendings.data

import com.kovidinvo.trackspendings.R

class FormatUtils {
    companion object {
        fun formatAmount(am:Double) : String {
            return String.format("%,.2f",am)
        }
        fun formatCardNum(cn:Int) : String {
            return if(cn>0) String.format("%04d",cn) else "0000"
        }
    }
}