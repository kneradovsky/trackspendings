package com.kovidinvo.trackspendings.data

import android.content.Context
import com.kovidinvo.trackspendings.R

enum class Bank(val n:String) {
    SBERBANK("Sberbank"),
    OTKRITIE("OTKRITIE"),
    TINKOFF("Tinkoff"),
    ALFABANK("Alfa-Bank"),
    VTB("VTB");

   companion object {
       fun bankByStr1(str:String): Bank? {
            return Bank.values().find {it.n==str}
       }
   }
}

class BankData(val ctx:Context) {


    companion object {

        val logosList = mapOf(
                Bank.SBERBANK.n to R.drawable.ic_banklogo_sberbank,
                Bank.OTKRITIE.n to R.drawable.ic_banklogo_otkritie,
                Bank.TINKOFF.n to R.drawable.ic_banklogo_tinkoff,
                Bank.ALFABANK.n to R.drawable.ic_banklogo_alfabank,
                Bank.VTB.n to R.drawable.ic_banklogo_vtb
        )
        val namesLocal = mapOf (
                Bank.ALFABANK.n to R.string.alfa_bank,
                Bank.TINKOFF.n to R.string.tinkoff_bank,
                Bank.OTKRITIE.n to R.string.otkritie_bank,
                Bank.SBERBANK.n to R.string.sberbank_bank,
                Bank.VTB.n to R.string.vtb_bank
        )

    }
}