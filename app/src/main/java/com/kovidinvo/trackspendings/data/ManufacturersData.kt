package com.kovidinvo.trackspendings.data

enum class Manufacturer(m:String) {
    XIAOMI("xiaomi"),
    MEIZU("meizu"),
    HUAWEI("huawei"),
    SAMSUNG("samsung")

}

class ManufacturersData {
    companion object {

    }
}