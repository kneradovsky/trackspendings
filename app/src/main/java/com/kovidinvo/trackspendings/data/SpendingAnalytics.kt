package com.kovidinvo.trackspendings.data

data class SpendAnalytics(
        val name:String,
        val chartType: String = "pie",
        val periodType : String = "day",
        val begin : Long = 0L,
        val end : Long = 0L
        )


enum class PeriodType(val pt:String) {
    DAY("day"),
    WEEK("week"),
    MONTH("month"),
    CUSTOM("custom")
}


enum class SpendChartType(val ct:String) {
    PIE("pie"),
    BAR("bar"),
    BARSTACK("barstack"),
    LINE("line"),
    LINESTACK("linestack")
}
